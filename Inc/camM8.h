/*
 * camM8.h
 *
 *  Created on: 6 de mai de 2020
 *      Author: oscar
 */

#ifndef CAMM8_H_
#define CAMM8_H_


#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

UART_HandleTypeDef hlpuart1;

uint8_t GPS_flag_get_values_ok;
unsigned int latitude;
unsigned int longitude;
unsigned int altitude;
unsigned int hour;
unsigned int minute;
unsigned int second;
unsigned int numSV;
unsigned int quality;
unsigned int speed;



void fn_get_gps_camM8();

unsigned int fn_get_latutude_camM8();

unsigned int fn_get_longitude_camM8();

unsigned int fn_get_altitude_camM8();

unsigned int fn_get_hour_camM8();

unsigned int fn_get_minute_camM8();

unsigned int fn_get_second_camM8();

unsigned int fn_get_numSV_camM8();

unsigned int fn_get_quality_camM8();

unsigned int fn_get_speed_camM8();

uint8_t fn_get_gps_values_camM8();

uint32_t secondsToDayTimeGPS();

uint8_t fn_check_camM8_status();

void fn_check_gps_program();

uint32_t fn_gps_timer_on();

#endif /* CAMM8_H_ */
