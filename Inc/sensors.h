/*
 * sensors.h
 *
 *  Created on: 16 de mar de 2020
 *      Author: oscar
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#define VOL_MIN		20

typedef struct {
	//e_uplink_frames_type_t header;
	unsigned int angle, battery, distance, temperature, latitude, longitude,
			referenceVol, volume;

} st_sensors_data_t;

st_sensors_data_t st_data_sensor_e;
st_sensors_data_t st_data_sensor_previwes_e;

struct{
	char
	temperature[4],
	angle[2],
	battery[4],
	latitude[6],
	longitude[6],
	referenceVol[4],
	volume[2],
	distance[4];
}st_sensor_char_values;


void fn_init_sensors();
void fn_get_sensors_values();
void fn_get_angle_value();
void fn_get_volume_value();
void fn_get_latitude_value();
void fn_get_longitude_value();
void fn_get_temperature_value();
void fn_get_battery_value();
void fn_get_lat_lon_values();
void fn_get_sensors_char_values();

#endif /* SENSORS_H_ */
