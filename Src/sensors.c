/*
 * sensors.c
 *
 *  Created on: 16 de mar de 2020
 *      Author: oscar
 */

#include "sensors.h"

void fn_init_sensors() {
	fn_init_lsm303ah();
	fn_info_sigfox();
	//fn_init_gps();
	st_data_sensor_e.angle = 0;
	st_data_sensor_e.battery = 0;
	st_data_sensor_e.distance = 0;
	st_data_sensor_e.latitude = 0;
	st_data_sensor_e.longitude = 0;
	st_data_sensor_e.referenceVol = 200;
	st_data_sensor_e.temperature = 0;
}

void fn_get_sensors_values() {
	fn_get_angle_value();
	fn_get_volume_value();
	fn_get_lat_lon_values();
	fn_get_temperature_value();
	fn_get_battery_value();
}

void fn_get_sensors_char_values() {

	fn_get_angle_value();
	HAL_Delay(100);
	fn_get_volume_value();
	HAL_Delay(100);
	fn_get_lat_lon_values();
	fn_get_temperature_value();
	HAL_Delay(100);
	fn_get_battery_value();
	HAL_Delay(100);

	int temp_bff =  st_stm_adc_variables.temperature * 100;
	int volts_buff = st_stm_adc_variables.battery * 1000;
	int dist_buff = st_data_sensor_e.distance;

	itoa((int) st_data_sensor_e.angle, st_sensor_char_values.angle, 10);
	itoa(volts_buff, st_sensor_char_values.battery,10);
	itoa(dist_buff , st_sensor_char_values.distance, 10);
	itoa((int) st_data_sensor_e.latitude, st_sensor_char_values.latitude, 10);
	itoa((int) st_data_sensor_e.longitude, st_sensor_char_values.longitude, 10);
	itoa((int) st_data_sensor_e.referenceVol,
			st_sensor_char_values.referenceVol, 10);
	itoa(temp_bff,st_sensor_char_values.temperature, 10);
	itoa((int) st_data_sensor_e.volume, st_sensor_char_values.volume, 10);

}


//**************** PROCESSING SENSORS VALUES *********************
void fn_get_angle_value() {
	st_data_sensor_previwes_e.angle = st_data_sensor_e.angle;
	fn_get_lsm303ah();
	if (st_accelerometer.pitch < 0)
		st_accelerometer.pitch *= (-1);
	st_data_sensor_e.angle = round(st_accelerometer.pitch / 10);
}

void fn_get_volume_value() {
	st_data_sensor_previwes_e.distance = st_data_sensor_e.distance;
	st_data_sensor_e.distance = fn_get_dist_sen031x();

}

void fn_get_lat_lon_values() {
	st_data_sensor_previwes_e.latitude = st_data_sensor_e.latitude;
	st_data_sensor_previwes_e.longitude = st_data_sensor_e.longitude;
	fn_get_longitude_value();
	fn_get_latitude_value();
}

void fn_get_latitude_value() {
	//st_data_sensor_e.latitude = st_gps_data_current.latitude;
}

void fn_get_longitude_value() {
	//st_data_sensor_e.longitude = st_gps_data_previous.longitude;
}

void fn_get_temperature_value() {
	st_data_sensor_previwes_e.temperature = st_data_sensor_e.temperature;
	fn_get_stm32_temperature();
	/*SIGFOX_ON
	HAL_Delay(1000);
	if (fn_at_sigfox() == 0)
		fn_at_sigfox();
	fn_get_stm32_temperature();
	fn_get_temperature_sigfox();
	SIGFOX_OFF
	if (st_stm_adc_variables.temperature != 0 && int_temp_sigfox != 0) {
		st_data_sensor_e.temperature = int_temp_sigfox;
	} else if (st_stm_adc_variables.temperature == 0 && int_temp_sigfox != 0) {
		st_data_sensor_e.temperature = int_temp_sigfox;
	} else if (st_stm_adc_variables.temperature != 0 && int_temp_sigfox == 0) {
		st_data_sensor_e.temperature = st_stm_adc_variables.temperature;
	}*/
	st_data_sensor_e.temperature = st_stm_adc_variables.temperature;
}

void fn_get_battery_value() {
	st_data_sensor_previwes_e.battery = st_data_sensor_e.battery;
	fn_get_stm32_volts();
	st_data_sensor_e.battery = (st_stm_adc_variables.battery/10)-120;
	/*SIGFOX_ON
	HAL_Delay(1000);
	if (fn_at_sigfox() == 0)
		fn_at_sigfox();
	fn_get_stm32_volts();
	fn_get_volt_sigfox();
	SIGFOX_OFF

	if (st_stm_adc_variables.battery == 0) {
		st_data_sensor_e.battery = int_volt_sigfox-120;
	} else {
		st_data_sensor_e.battery = (st_stm_adc_variables.battery/10)-120;
	}*/

}
