/*
 * encoder.c
 *
 *  Created on: 9 de mar de 2020
 *      Author: oscar
 */

#include "encoder.h"

//########################## ENCODER  PAYLOAD ###############################

void fn_encoder_report_frame(char * frame) {

	//char report_frame_complete[31] = { 0 };

	char buffer_longitude[7] = { 0 };
	char buffer_latitude[7] = { 0 };
	char buffer_distance[7] = { 0 };
	char buffer_temperature[7] = { 0 };
	char buffer_battery[7] = { 0 };
	//char buffer_angle[7] = { 0 };
	char header_buffer[2] = "2";
	//itoa(header, header_char, 10);
	/*	report_frame_complete[6] = 48;
	 report_frame_complete[7] = 50;*/

	check_size_info(2, header_buffer);
	strcat(frame, header_buffer);

	decHex(st_data_sensor_e.battery, buffer_battery);
	check_size_info(2, buffer_battery);
	strcat(frame, buffer_battery);

	decHex(st_data_sensor_e.temperature, buffer_temperature);
	check_size_info(2, buffer_temperature);
	strcat(frame, buffer_temperature);

	decHex(st_data_sensor_e.latitude, buffer_latitude);
	check_size_info(6, buffer_latitude);
	strcat(frame, buffer_latitude);

	decHex(st_data_sensor_e.longitude, buffer_longitude);
	check_size_info(6, buffer_longitude);
	strcat(frame, buffer_longitude);

	decHex(st_data_sensor_e.distance, buffer_distance);
	check_size_info(2, buffer_distance);
	strcat(frame, buffer_distance);

/*	decHex(st_data_sensor_e.angle, buffer_angle);
	check_size_info(2, buffer_angle);
	strcat(frame, buffer_angle);*/

	strcat(frame, ble_distance);

}

//########################## ENCODER HEXA/DEC/BIN ###############################
int OnehextoOneDec(char hex[]) {
	int i = 0;
	int decimal = 0;
	/* Find the decimal representation of hex[i] */
	if (hex[i] >= '0' && hex[i] <= '9') {
		decimal = hex[i] - 48;
	} else if (hex[i] >= 'a' && hex[i] <= 'f') {
		decimal = hex[i] - 97 + 10;
	} else if (hex[i] >= 'A' && hex[i] <= 'F') {
		decimal = hex[i] - 65 + 10;
	}

	return decimal;
}

int hexDec(char *hex) {
	int i = 0;
	int val = 0;
	int len;

	int decimal = 0;

	/* Find the length of total number of hex digit */
	len = strlen(hex);
	len--;

	/*
	 * Iterate over each hex digit
	 */
	for (i = 0; i < strlen(hex); i++) {

		/* Find the decimal representation of hex[i] */
		if (hex[i] >= '0' && hex[i] <= '9') {
			val = hex[i] - 48;
		} else if (hex[i] >= 'a' && hex[i] <= 'f') {
			val = hex[i] - 97 + 10;
		} else if (hex[i] >= 'A' && hex[i] <= 'F') {
			val = hex[i] - 65 + 10;
		}

		decimal += val * pow(16, len);
		len--;
	}
	return decimal;
}

void decHex(uint32_t number, char buff[7]) {

	char hex[7]; /*bcoz it contains characters A to F*/
	memset(buff, 0x00, 7); //sizeof(buff));
	memset(hex, 0x00, 7); //sizeof(buff));
	int cnt, i;
	cnt = 0; /*initialize index to zero*/
	if (number == 0) {
		strcpy(buff, "0");
	}
	while (number > 0) {
		switch (number % 16) {
		case 10:
			hex[cnt] = 'A';
			break;
		case 11:
			hex[cnt] = 'B';
			break;
		case 12:
			hex[cnt] = 'C';
			break;
		case 13:
			hex[cnt] = 'D';
			break;
		case 14:
			hex[cnt] = 'E';
			break;
		case 15:
			hex[cnt] = 'F';
			break;
		default:
			hex[cnt] = (number % 16) + 0x30; /*converted into char value*/
		}
		number = number / 16;
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--) {
		int j = cnt - 1 - i;
		buff[j] = hex[i];
	}
}

void decToBinary(int number, char binary[32]) {
	int cnt, i;
	int bin[32];
	cnt = 0; /*initialize index to zero*/
	while (number > 0) {
		bin[cnt] = number % 2;
		number = number / 2;
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--)
		binary[i] = bin[i];
}

int num_hex_digits(unsigned n) {
	if (!n)
		return 1;

	int ret = 0;
	for (; n; n >>= 4) {
		++ret;
	}
	return ret;
}

void hexBin(char *hex, char *dec) {

	int i = 0, size;
	memset(dec, '\0', strlen(dec));
	size = strlen(hex);
	char new_hex[size];
	strcpy(new_hex, hex);
	/* Extract first digit and find binary of each hex digit */
	for (i = 0; i < size; i++) {
		switch (new_hex[i]) {
		case '0':
			strcat(dec, "0000");
			break;
		case '1':
			strcat(dec, "0001");
			break;
		case '2':
			strcat(dec, "0010");
			break;
		case '3':
			strcat(dec, "0011");
			break;
		case '4':
			strcat(dec, "0100");
			break;
		case '5':
			strcat(dec, "0101");
			break;
		case '6':
			strcat(dec, "0110");
			break;
		case '7':
			strcat(dec, "0111");
			break;
		case '8':
			strcat(dec, "1000");
			break;
		case '9':
			strcat(dec, "1001");
			break;
		case 'a':
		case 'A':
			strcat(dec, "1010");
			break;
		case 'b':
		case 'B':
			strcat(dec, "1011");
			break;
		case 'c':
		case 'C':
			strcat(dec, "1100");
			break;
		case 'd':
		case 'D':
			strcat(dec, "1101");
			break;
		case 'e':
		case 'E':
			strcat(dec, "1110");
			break;
		case 'f':
		case 'F':
			strcat(dec, "1111");
			break;
		default:
			break;
		}
	}
}

int binaryToDec(int num) {
	int decimal_val = 0, base = 1, rem = 0;

	//printf("Enter a binary number(1s and 0s) \n");
	//scanf("%d", &num); /* maximum five digits */
	//int binary_val = num;
	while (num > 0) {
		rem = num % 10;
		decimal_val = decimal_val + rem * base;
		num = num / 10;
		base = base * 2;
	}
	return decimal_val;
}

int bin_to_dec(char *bin) {

	int i, tam, novoValor = 0;
	tam = strlen(bin); //verifica quantos d�gitos tem no n�mero

	//pega os d�gitos da direita para a esquerda
	for (i = tam - 1; i >= 0; i--) {
		//printf("%c|", numero[i]);
		if (bin[i] == '1') {
			novoValor += pow(2, tam - 1 - i);
		}
	}

	return novoValor;
}
//########################## ENCODER AUXILIAR PAYLOAD ###############################
void RemoveSpaces(char source[]) {
	//int tam = strlen(source);
	char* i = source;
	char* j = source;

	while (*j != 0) {
		*i = *j++;
		if (*i != ' ')
			i++;
	}
	*i = 0;

}

void check_size_info(int size, char*buff) {
	if (size == 2) {
		char dado[4] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			//dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			/*			dado[0] = 48;
			 strcat(dado, buff);
			 strcpy(buff, dado);*/
			break;
			/*		case (3):
			 break;*/
		default:
			buff[0] = 48;
			buff[1] = 48;
			break;
		}
	} else if (size == 4) {
		char dado[5] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			dado[0] = 48;
			dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (3):
			dado[0] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (4):
			break;
		default:
			buff[0] = 48;
			buff[1] = 48;
			buff[2] = 48;
			buff[3] = 48;
			buff[4] = 0;
			break;
		}
	} else if (size == 6) {
		char dado[7] = "";
		switch (strlen(buff)) {
		case (1):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			dado[3] = 48;
			dado[4] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (2):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			dado[3] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (3):
			dado[0] = 48;
			dado[1] = 48;
			dado[2] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (4):
			dado[0] = 48;
			dado[1] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (5):
			dado[0] = 48;
			strcat(dado, buff);
			strcpy(buff, dado);
			break;
		case (6):
			break;
		default:
			buff[0] = 48;
			buff[1] = 48;
			buff[2] = 48;
			buff[3] = 48;
			buff[4] = 48;
			buff[5] = 48;
			buff[6] = 0;
			break;
		}

	}
}

int fn_get_seconsForTimeStemp(int TS_Total) {
	//TS_Total = 355195;
	int TS_hour, TS_day, TS_minute, TS_secons;
	float TS_hour_aux, TS_day_aux, TS_minute_aux;

	TS_day_aux = TS_Total / 1440.000;
	TS_day = TS_day_aux;

	TS_hour_aux = (TS_day_aux - TS_day) * 24.000;
	TS_hour = TS_hour_aux;

	TS_minute_aux = (TS_hour_aux - TS_hour) * 60.00;
	TS_minute = round(TS_minute_aux);

	TS_secons = 60 * (TS_minute + (TS_hour * 60));

	return TS_secons;
}
//########################## ENCODER AUXILIAR GPS ###############################

uint8_t find_between(const char *first, const char *last, char *buff,
		char *buff_return) {
	uint8_t ok = 0;
	//const char *last = "*";
	//const char *buff = _gps;
	char *target = NULL;
	char *start, *end;
	start = strstr(buff, first);
	end = strstr(start, last);

	if (start) {
		start += strlen(first);
		if (end) {
			target = (char *) malloc(end - start + 1);
			memcpy(target, start, end - start);
			target[end - start] = '\0';
			ok = 1;
		}
	}
	strcpy(buff_return, target);
	free(target);
	return ok;
}

int find_word(char *where, char *word) {
	int size_where = strlen(where);
	int size_word = strlen(word);
	int ok = 0;
	int n = 0;
	int m = 0;

	for (int var = 0; var < size_where; ++var) {
		// if first character of search string matches
		while (where[n] == word[m]) {
			n++;
			m++;
		}
		// if we sequence of characters matching with the length of searched string
		if (m == size_word) {
			ok = 1;
			break;
		}
		n++;
		m = 0; // reset the counter to start from first character of the search string.
	}

	return ok;
}

int countOccurrences(char * str, char * toSearch) {
	int i, j, found, count;
	int stringLen, searchLen;

	stringLen = strlen(str);      // length of string
	searchLen = strlen(toSearch); // length of word to be searched

	count = 0;

	for (i = 0; i <= stringLen - searchLen; i++) {
		/* Match word with string */
		found = 1;
		for (j = 0; j < searchLen; j++) {
			if (str[i + j] != toSearch[j]) {
				found = 0;
				break;
			}
		}

		if (found == 1) {
			count++;
		}
	}

	return count;
}

//########################## DECODER AUXILIAR GPRS ###############################

uint8_t get_downlink_payload(char* data) {

	uint8_t responce = 0;

	memset(st_frame_type.downlink_data, 0, 16);

	int size_w = strlen(data);

	for (int var = 0; var < size_w; ++var) {
		if (data[var + 1] == 88 && data[var + 2] == 61) {
			st_frame_type.downlink_data[0] = data[var + 3];
			st_frame_type.downlink_data[1] = data[var + 4];
			st_frame_type.downlink_data[2] = data[var + 5];
			st_frame_type.downlink_data[3] = data[var + 6];
			st_frame_type.downlink_data[4] = data[var + 7];
			st_frame_type.downlink_data[5] = data[var + 8];
			st_frame_type.downlink_data[6] = data[var + 9];
			st_frame_type.downlink_data[7] = data[var + 10];
			st_frame_type.downlink_data[8] = data[var + 11];
			st_frame_type.downlink_data[9] = data[var + 12];
			st_frame_type.downlink_data[10] = data[var + 13];
			st_frame_type.downlink_data[11] = data[var + 14];
			st_frame_type.downlink_data[12] = data[var + 15];
			st_frame_type.downlink_data[13] = data[var + 16];
			st_frame_type.downlink_data[14] = data[var + 17];
			st_frame_type.downlink_data[15] = data[var + 18];
			responce = 1;
			break;
		}
	}
	fn_fprint("\r\nDOWNLINK: ");
	fn_fprint(st_frame_type.downlink_data);
	fn_fprint("\r\n");

	char RX_buff_bin[64] = { 0 };
	char volume_ref[3] = { 0 };
	/*
	 char gas_thresh[2] = { 0 };
	 char vol_thresh[2] = { 0 };
	 char ang_thresh[2] = { 0 };
	 char tmp_thresh[2] = { 0 };*/
	char timebyte[6] = { 0 };
	char undTimeByte[2] = { 0 };
	char time_stamp[6] = { 0 };

	int TimeStamp = 0, timebyte_int = 0, undTimeByte_int = 0, time_byte_value =
			0;

	/*	size = strlen(RX);
	 for (int var = 0; var < size; ++var) {
	 if (RX[var] == 84 && RX[var + 1] == 88 && RX[var + 2] == 61) {
	 downlinkRX[0] = RX[var + 3];
	 downlinkRX[1] = RX[var + 4];
	 downlinkRX[2] = RX[var + 5];
	 downlinkRX[3] = RX[var + 6];
	 downlinkRX[4] = RX[var + 7];
	 downlinkRX[5] = RX[var + 8];
	 downlinkRX[6] = RX[var + 9];
	 downlinkRX[7] = RX[var + 10];
	 downlinkRX[8] = RX[var + 11];
	 downlinkRX[9] = RX[var + 12];
	 downlinkRX[10] = RX[var + 13];
	 downlinkRX[11] = RX[var + 14];
	 downlinkRX[12] = RX[var + 15];
	 downlinkRX[13] = RX[var + 16];
	 downlinkRX[14] = RX[var + 17];
	 downlinkRX[15] = RX[var + 18];
	 }
	 }*/

	if (responce == 1) {
		time_stamp[0] = st_frame_type.downlink_data[6];
		time_stamp[1] = st_frame_type.downlink_data[7];
		time_stamp[2] = st_frame_type.downlink_data[4];
		time_stamp[3] = st_frame_type.downlink_data[5];
		time_stamp[4] = st_frame_type.downlink_data[2];
		time_stamp[5] = st_frame_type.downlink_data[3];

		volume_ref[0] = st_frame_type.downlink_data[10];
		volume_ref[1] = st_frame_type.downlink_data[11];
		/*
		 vol_thresh[0] = downlinkRX[15];
		 ang_thresh[0] = downlinkRX[14];
		 tmp_thresh[0] = downlinkRX[13];
		 gas_thresh[0] = downlinkRX[12];*/
		HAL_Delay(10);
		hexBin(st_frame_type.downlink_data, RX_buff_bin);

		fn_fprint("\r\nDOWNLINK BINARY: ");
		fn_fprint(RX_buff_bin);

		//choice of transmission resource
		if (RX_buff_bin[4] == 49)
			st_flag.SigFox = 1;
		if (RX_buff_bin[4] == 48)
			st_flag.SigFox = 0;
		if (RX_buff_bin[3] == 49)
			st_flag.GPRS = 1;
		if (RX_buff_bin[3] == 48)
			st_flag.GPRS = 0;
		if (RX_buff_bin[2] == 49)
			st_flag.BLE = 1;
		if (RX_buff_bin[2] == 48)
			st_flag.BLE = 0;

		undTimeByte[0] = RX_buff_bin[32]; //0
		undTimeByte[1] = RX_buff_bin[33]; //1
		timebyte[0] = RX_buff_bin[34]; //0
		timebyte[1] = RX_buff_bin[35]; //1
		timebyte[2] = RX_buff_bin[36]; //1
		timebyte[3] = RX_buff_bin[37]; //1
		timebyte[4] = RX_buff_bin[38]; //1
		timebyte[5] = RX_buff_bin[39]; //0
		HAL_Delay(10);
		timebyte_int = atoi(timebyte);
		HAL_Delay(10);
		undTimeByte_int = atoi(undTimeByte);
		HAL_Delay(10);
		time_byte_value = binaryToDec(timebyte_int); //transmission period in hours
		HAL_Delay(10);
		distance_threshold = hexDec(volume_ref);
		/*int Vol_Threshold = hexDec(vol_thresh);
		 int Gas_Threshold = hexDec(gas_thresh);
		 int Ang_Threshold = hexDec(ang_thresh);
		 int Tmp_Threshold = hexDec(tmp_thresh);*/

		TimeStamp = hexDec(time_stamp); //timestamp minutes-sigmais protocol

		TimeCounter = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day
		HAL_Delay(10);

		if (time_byte_value != 0) {
			if (undTimeByte_int == 00) //if 00 = time unid its seconds
					{
				enterKeepAliveTimer = time_byte_value;
			}
			if (undTimeByte_int == 01) // if 01 = time unid its minutes
					{
				enterKeepAliveTimer = (time_byte_value * 60);
			}
			if (undTimeByte_int == 10) // if 10 = time unid its hours
					{
				enterKeepAliveTimer = (time_byte_value * 3600);
			}
			if (undTimeByte_int == 11) // if 11 = time unid its days
					{
				enterKeepAliveTimer = (time_byte_value * 86400);
			}
		}
	}
	return responce;
}

//########################## DECODER AUXILIAR BLE ###############################

int find_location_word(char *where, char *word) {
	int size_where = strlen(where);
	int size_word = strlen(word);
	int retourn = 0;

	int n = 0;
	int m = 0;

	for (int var = 0; var < size_where; ++var) {
		// if first character of search string matches
		while (where[n] == word[m]) {
			n++;
			m++;
		}
		// if we sequence of characters matching with the length of searched string
		if (m == size_word) {
			retourn = n;
			break;
		}
		n++;
		m = 0; // reset the counter to start from first character of the search string.
	}

	return retourn;
}

void fn_encoder_ble_values(char* retorno){

	fn_get_stm32_temperature();
	fn_get_stm32_volts();
	uint32_t distancias = fn_get_dist_sen031x();


	char dados[44] = {0};
	char bateria[4]={0};
	char temperatura_1[2]={0};
	char temperatura_2[2]="00";
	char distancia[8] = {0};
	char time[8] = {0};


	decHexBLE(st_stm_adc_variables.battery,bateria,4);
	decHexBLE(st_stm_adc_variables.temperature,temperatura_1,2);
	decHexBLE(distancias,distancia,8);
	decHexBLE(TimeCounter,time,8);

	strncpy(dados,"0303AAFE1116AAFE2000",20);
	strncat(dados,bateria,4);
	strncat(dados,temperatura_1,2);
	strncat(dados,temperatura_2,2);
	strncat(dados,distancia,8);
	strncat(dados,time,8);

	strcpy(retorno,dados);
}

void decHexBLE(uint32_t number, char* buff, uint32_t size) {

	char hex[size]; /*bcoz it contains characters A to F*/
	memset(buff, 0x00, size); //sizeof(buff));
	memset(hex, 0x00, size); //sizeof(buff));
	int cnt, i;
	cnt = 0; /*initialize index to zero*/
	if (number == 0) {
		strcpy(buff, "0");
	}
	while (number > 0) {
		switch (number % 16) {
		case 10:
			hex[cnt] = 'A';
			break;
		case 11:
			hex[cnt] = 'B';
			break;
		case 12:
			hex[cnt] = 'C';
			break;
		case 13:
			hex[cnt] = 'D';
			break;
		case 14:
			hex[cnt] = 'E';
			break;
		case 15:
			hex[cnt] = 'F';
			break;
		default:
			hex[cnt] = (number % 16) + 0x30; /*converted into char value*/
		}
		number = number / 16;
		cnt++;
	}
	while(cnt!=size){

		hex[cnt] = '0';
		cnt++;
	}
	for (i = (cnt - 1); i >= 0; i--) {
		int j = cnt - 1 - i;
		buff[j] = hex[i];
	}

}
