/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 * @by oscar machado de souza
 * @init - 09/03/2020
 * @project - wizebox
 * @company - Wize Company LTDA
 * @Hardware version - 3.1 (by Bruno Fernandes)
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* Definition of ADCx conversions data table size */
/* Size of array set to ADC sequencer number of ranks converted,            */
/* to have a rank in each array address.*/
#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)   3)
/* Definitions of environment analog values */
/* Value of analog reference voltage (Vref+), connected to analog voltage   */
/* supply Vdda (unit: mV).                                                  */
#define VDDA_APPLI                       ((uint32_t)3300)
#define ADC_CALIBRATION_TIMEOUT_MS       ((uint32_t)   1)
#define ADC_ENABLE_TIMEOUT_MS            ((uint32_t)   1)
#define ADC_DISABLE_TIMEOUT_MS           ((uint32_t)   1)
#define ADC_STOP_CONVERSION_TIMEOUT_MS   ((uint32_t)   1)
#define ADC_CONVERSION_TIMEOUT_MS        ((uint32_t) 300)

/* Delay between ADC end of calibration and ADC enable.                     */
/* Delay estimation in CPU cycles: Case of ADC enable done                  */
/* immediately after ADC calibration, ADC clock setting slow                */
/* (LL_ADC_CLOCK_ASYNC_DIV32). Use a higher delay if ratio                  */
/* (CPU clock / ADC clock) is above 32.                                     */
#define ADC_DELAY_CALIB_ENABLE_CPU_CYCLES  (LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES * 32)

/* Prescaler declaration */
uint32_t uwPrescalerValue = 0;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

I2C_HandleTypeDef hi2c1;

LPTIM_HandleTypeDef hlptim1;

UART_HandleTypeDef hlpuart1;
UART_HandleTypeDef huart2;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
/* Variables for ADC conversion data */
__IO uint16_t aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE]; /* ADC group regular conversion data */

/* Variable to report status of DMA transfer of ADC group regular conversions */
/*  0: DMA transfer is not completed                                          */
/*  1: DMA transfer is completed                                              */
/*  2: DMA transfer has not been started yet (initial state)                  */
__IO uint8_t ubDmaTransferStatus = 2; /* Variable set into DMA interruption callback */

/* Variable to report status of ADC group regular sequence conversions:       */
/*  0: ADC group regular sequence conversions are not completed               */
/*  1: ADC group regular sequence conversions are completed                   */
__IO uint8_t ubAdcGrpRegularSequenceConvStatus = 0; /* Variable set into ADC interruption callback */

/* Variable to report number of ADC group regular sequence completed          */
static uint32_t ubAdcGrpRegularSequenceConvCount = 0; /* Variable set into ADC interruption callback */

/* Variables for ADC conversion data computation to physical values */
__IO uint16_t uhADCxConvertedData_VoltageGPIO_mVolt = 0; /* Value of voltage on GPIO pin (on which is mapped ADC channel) calculated from ADC conversion data (unit: mV) */
__IO uint16_t uhADCxConvertedData_VrefInt_mVolt = 0; /* Value of internal voltage reference VrefInt calculated from ADC conversion data (unit: mV) */
__IO int16_t hADCxConvertedData_Temperature_DegreeCelsius = 0; /* Value of temperature calculated from ADC conversion data (unit: degree Celcius) */
__IO uint16_t uhADCxConvertedData_VrefAnalog_mVolt = 0; /* Value of analog reference voltage (Vref+), connected to analog voltage supply Vdda, calculated from ADC conversion data (unit: mV) */

uint32_t timer = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_I2C1_Init(void);
static void MX_LPUART1_UART_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART4_UART_Init(void);
static void MX_USART5_UART_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM2_Init(void);
static void MX_LPTIM1_Init(void);
/* USER CODE BEGIN PFP */
float get_temp(uint32_t variable);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */

int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_I2C1_Init();
	MX_LPUART1_UART_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_USART4_UART_Init();
	MX_USART5_UART_Init();
	MX_RTC_Init();
	MX_TIM2_Init();
	MX_LPTIM1_Init();
	/* USER CODE BEGIN 2 */
	blink(25);
	LED_ON
	HAL_Delay(3000);
	fn_fprint("START PROGRAM\r\n");
	LED_OFF
	e_Machine_States_t = INITIALIZING;
	LED_ON
	//
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
		switch (e_Machine_States_t) {
		case NONE:
			break;
		case INITIALIZING:

			fn_check_sensors_status_program();
			fn_check_communication_status_program();

			st_flag.SigFox = 0;
			st_flag.GPRS = 1;
			st_flag.BLE = 0;
			st_flag.LoRa = 0;
			st_flag.alarm = 0;

			enterKeepAliveTimer = 60; //TIMER_KEEP_ALIVE_TIMER_INIT;
			TimeCounter = 84000;
			e_Machine_States_t = SEND_INFO_FRAME;

			fn_fprint("\r\nLOOPING\r\n");
			break;
		case ERRO:

			break;
		case VERIFY:

			//e_Machine_States_t = LPM;

			if (TimeCounter % READ_SENSORS_TIMER == 0) {
				//fn_exit_lowPowerMode();
				e_Machine_States_t = READ_SENSOR;
			}
			if (TimeCounter % enterKeepAliveTimer == 0) {
				//fn_exit_lowPowerMode();
				fn_fprint("\r\n INFO FRAME \r\n");
				e_Machine_States_t = SEND_INFO_FRAME;
			}
			if (TimeCounter >= SECONS_ONE_DAY) {
				//fn_exit_lowPowerMode();
				fn_fprint("\r\n DAILY FRAME \r\n");
				TimeCounter = 0;
				e_Machine_States_t = READ_GPS;
			}
			if (TimeCounter % READ_LSM_TIMER == 0) {
				if (st_flag.acelerometer == 1) {
					//fn_exit_lowPowerMode();
					//LED_ON
					//fn_print_timers_values();
					//fn_get_angle_value();
					//fn_print_lsm_values();
					if (st_data_sensor_e.angle >= 3) {
						fn_fprint("\r\n ANGLE ALARM!!\r\n");
						//e_Machine_States_t = SEND_ALERT_FRAME;
					}
					if (st_magnetometer.m_z > 1500) {
						//e_Machine_States_t = BLE_ENTER;
					}
				}

			}

			break;
		case LPM:
			//COLOCAR AQUI O CODIGO DO LOW POWER MODE
			LED_OFF
			//fn_enter_lowPowerMode();
			e_Machine_States_t = VERIFY;
			break;
		case SEND_DAILY_FRAME:
			st_flag.alarm = 0;
			if (st_flag.BLE == 1) {
				e_Current_nina_config_type = EDDYSTONE_BEACON_MODE;
				fn_main_nina();
			}
			fn_get_sensors_values();
			fn_print_sensor_values();
			if (GPS_flag_get_values_ok == 0) {
				fn_check_gps_program();
			}

			fn_send_payload(DAILY_UPDATE_FRAME);
			//fn_send_payload(TAGO_FRAME);

			e_Machine_States_t = LPM;
			break;
		case SEND_INFO_FRAME:
			if (st_flag.BLE == 1) {
				e_Current_nina_config_type = EDDYSTONE_BEACON_MODE;
				fn_main_nina();
			}
			fn_get_sensors_values();
			fn_print_sensor_values();
			fn_send_payload(INFO_FRAME_PROCESSED);
			//fn_send_payload(TAGO_FRAME);

			e_Machine_States_t = LPM;
			break;
		case SEND_ALERT_FRAME:
			fn_send_payload(INFO_FRAME_PROCESSED);
			e_Machine_States_t = LPM;
			break;
		case READ_SENSOR:
			fn_get_sensors_values();
			fn_print_sensor_values();
			if (st_flag.BLE == 1) {
				e_Current_nina_config_type = EDDYSTONE_BEACON_MODE;
				fn_main_nina();
			}
			if ((st_data_sensor_e.distance < 65
					|| st_stm_adc_variables.temperature > 80
					|| st_stm_adc_variables.battery <= 3400)
					&& st_flag.alarm == 0) {
				st_flag.alarm = 1;
				fn_fprint("\r\nSENSOR ALARM!!\r\n");
				e_Machine_States_t = SEND_ALERT_FRAME;
			} else
				e_Machine_States_t = LPM;
			break;
		case READ_GPS:
			if (st_flag.GPS == 1) {
				sum_gps_enable = 0;
				GPS_flag_get_values_ok = 0;
				fn_fprint("\r\nGPS DATA\r\n\r\n\r\n");
				GPS_ON
				while (GPS_flag_get_values_ok == 0) {
					sum_gps_enable++;
					GPS_flag_get_values_ok = fn_get_gps_values_camM8();
					if (latitude != 0 && longitude != 0) {
						if (TIMER_ENABLE_GPS == sum_gps_enable) {
							GPS_flag_get_values_ok = 1;
						}
					} else {
						if (sum_gps_enable == 300) {
							GPS_flag_get_values_ok = 1;
						}
					}
				}
				GPS_OFF
			}
			e_Machine_States_t = SEND_DAILY_FRAME;
			break;
		case BLE_ENTER:
			break;
		default:
			break;
		}
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI
			| RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.MSICalibrationValue = 0;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1
			| RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_LPUART1 | RCC_PERIPHCLK_I2C1
			| RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_LPTIM1;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_PCLK;

	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief ADC Initialization Function
 * @param None
 * @retval None
 */
static void MX_ADC_Init(void) {

	/* USER CODE BEGIN ADC_Init 0 */

	/* USER CODE END ADC_Init 0 */

	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = { 0 };
	LL_ADC_InitTypeDef ADC_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
	/**ADC GPIO Configuration
	 PA4   ------> ADC_IN4
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* ADC DMA Init */

	/* ADC Init */
	LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_1, LL_DMA_REQUEST_0);

	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_PRIORITY_HIGH);

	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);

	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_PERIPH_NOINCREMENT);

	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_MEMORY_INCREMENT);

	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_PDATAALIGN_HALFWORD);

	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1,
	LL_DMA_MDATAALIGN_HALFWORD);

	/* ADC interrupt Init */
	NVIC_SetPriority(ADC1_COMP_IRQn, 0);
	NVIC_EnableIRQ(ADC1_COMP_IRQn);

	/* USER CODE BEGIN ADC_Init 1 */
	/*## Configuration of NVIC #################################################*/
	/* Configure NVIC to enable DMA interruptions */
	NVIC_SetPriority(DMA1_Channel1_IRQn, 1); /* DMA IRQ lower priority than ADC IRQ */
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

	/*## Configuration of DMA ##################################################*/
	/* Enable the peripheral clock of DMA */
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

	/* Configure the DMA transfer */
	/*  - DMA transfer in circular mode to match with ADC configuration:        */
	/*    DMA unlimited requests.                                               */
	/*  - DMA transfer from ADC without address increment.                      */
	/*  - DMA transfer to memory with address increment.                        */
	/*  - DMA transfer from ADC by half-word to match with ADC configuration:   */
	/*    ADC resolution 12 bits.                                               */
	/*  - DMA transfer to memory by half-word to match with ADC conversion data */
	/*    buffer variable type: half-word.                                      */
	LL_DMA_ConfigTransfer(DMA1,
	LL_DMA_CHANNEL_1,
	LL_DMA_DIRECTION_PERIPH_TO_MEMORY |
	LL_DMA_MODE_CIRCULAR |
	LL_DMA_PERIPH_NOINCREMENT |
	LL_DMA_MEMORY_INCREMENT |
	LL_DMA_PDATAALIGN_HALFWORD |
	LL_DMA_MDATAALIGN_HALFWORD |
	LL_DMA_PRIORITY_HIGH);

	/* Select ADC as DMA transfer request */
	LL_DMA_SetPeriphRequest(DMA1,
	LL_DMA_CHANNEL_1,
	LL_DMA_REQUEST_0);

	/* Set DMA transfer addresses of source and destination */
	LL_DMA_ConfigAddresses(DMA1,
	LL_DMA_CHANNEL_1, LL_ADC_DMA_GetRegAddr(ADC1, LL_ADC_DMA_REG_REGULAR_DATA),
			(uint32_t) &aADCxConvertedData,
			LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

	/* Set DMA transfer size */
	LL_DMA_SetDataLength(DMA1,
	LL_DMA_CHANNEL_1,
	ADC_CONVERTED_DATA_BUFFER_SIZE);

	/* Enable DMA transfer interruption: transfer complete */
	LL_DMA_EnableIT_TC(DMA1,
	LL_DMA_CHANNEL_1);

	/* Enable DMA transfer interruption: transfer error */
	LL_DMA_EnableIT_TE(DMA1,
	LL_DMA_CHANNEL_1);

	/*## Activation of DMA #####################################################*/
	/* Enable the DMA transfer */
	LL_DMA_EnableChannel(DMA1,
	LL_DMA_CHANNEL_1);
	/* USER CODE END ADC_Init 1 */
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_4);
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_TEMPSENSOR);
	LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_PATH_INTERNAL_TEMPSENSOR);
	/** Configure Regular Channel
	 */
	LL_ADC_REG_SetSequencerChAdd(ADC1, LL_ADC_CHANNEL_VREFINT);
	LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_PATH_INTERNAL_VREFINT);
	/** Common config
	 */
	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_SOFTWARE;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_LIMITED;
	ADC_REG_InitStruct.Overrun = LL_ADC_REG_OVR_DATA_OVERWRITTEN;
	LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);
	LL_ADC_SetSamplingTimeCommonChannels(ADC1,
	LL_ADC_SAMPLINGTIME_160CYCLES_5);
	LL_ADC_SetOverSamplingScope(ADC1, LL_ADC_OVS_DISABLE);
	LL_ADC_REG_SetSequencerScanDirection(ADC1,
	LL_ADC_REG_SEQ_SCAN_DIR_FORWARD);
	LL_ADC_SetCommonFrequencyMode(__LL_ADC_COMMON_INSTANCE(ADC1),
	LL_ADC_CLOCK_FREQ_MODE_HIGH);
	LL_ADC_DisableIT_EOC(ADC1);
	LL_ADC_DisableIT_EOS(ADC1);
	LL_ADC_EnableInternalRegulator(ADC1);
	ADC_InitStruct.Clock = LL_ADC_CLOCK_SYNC_PCLK_DIV2;
	ADC_InitStruct.Resolution = LL_ADC_RESOLUTION_12B;
	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.LowPowerMode = LL_ADC_LP_MODE_NONE;
	LL_ADC_Init(ADC1, &ADC_InitStruct);
	/* USER CODE BEGIN ADC_Init 2 */
	__IO uint32_t wait_loop_index = 0;

	/*## Configuration of GPIO used by ADC channels ############################*/

	/* Note: On this STM32 device, ADC1 channel 4 is mapped on GPIO pin PA.04 */

	/* Enable GPIO Clock */
	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);

	/* Configure GPIO in analog mode to be used as ADC input */
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_4, LL_GPIO_MODE_ANALOG);

	/*## Configuration of NVIC #################################################*/
	/* Configure NVIC to enable ADC1 interruptions */
	NVIC_SetPriority(ADC1_COMP_IRQn, 0); /* ADC IRQ greater priority than DMA IRQ */
	NVIC_EnableIRQ(ADC1_COMP_IRQn);

	/*## Configuration of ADC ##################################################*/

	/*## Configuration of ADC hierarchical scope: common to several ADC ########*/

	/* Enable ADC clock (core clock) */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       All ADC instances of the ADC common group must be disabled.        */
	/* Note: In this example, all these checks are not necessary but are        */
	/*       implemented anyway to show the best practice usages                */
	/*       corresponding to reference manual procedure.                       */
	/*       Software can be optimized by removing some of these checks, if     */
	/*       they are not relevant considering previous settings and actions    */
	/*       in user application.                                               */
	if (__LL_ADC_IS_ENABLED_ALL_COMMON_INSTANCE() == 0) {
		/* Note: Call of the functions below are commented because they are       */
		/*       useless in this example:                                         */
		/*       setting corresponding to default configuration from reset state. */

		/* Set ADC clock (conversion clock) common to several ADC instances */
		/* Note: On this STM32 serie, ADC common clock asynchonous prescaler      */
		/*       is applied to each ADC instance if ADC instance clock is         */
		/*       set to clock source asynchronous                                 */
		/*       (refer to function "LL_ADC_SetClock()" below).                   */
		// LL_ADC_SetCommonClock(__LL_ADC_COMMON_INSTANCE(ADC1), LL_ADC_CLOCK_ASYNC_DIV1);
		/* Set ADC measurement path to internal channels */
		LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(ADC1),
				(LL_ADC_PATH_INTERNAL_VREFINT | LL_ADC_PATH_INTERNAL_TEMPSENSOR));

		/* Delay for ADC temperature sensor stabilization time.                   */
		/* Compute number of CPU cycles to wait for, from delay in us.            */
		/* Note: Variable divided by 2 to compensate partially                    */
		/*       CPU processing cycles (depends on compilation optimization).     */
		/* Note: If system core clock frequency is below 200kHz, wait time        */
		/*       is only a few CPU processing cycles.                             */
		/* Note: This delay is implemented here for the purpose in this example.  */
		/*       It can be optimized if merged with other delays                  */
		/*       during ADC activation or if other actions are performed          */
		/*       in the meantime.                                                 */
		wait_loop_index = ((LL_ADC_DELAY_TEMPSENSOR_STAB_US
				* (SystemCoreClock / (100000 * 2))) / 10);
		while (wait_loop_index != 0) {
			wait_loop_index--;
		}

		/*## Configuration of ADC hierarchical scope: multimode ####################*/

		/* Note: Feature not available on this STM32 serie */

	}

	/*## Configuration of ADC hierarchical scope: ADC instance #################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled.                                              */
	if (LL_ADC_IsEnabled(ADC1) == 0) {
		/* Note: Call of the functions below are commented because they are       */
		/*       useless in this example:                                         */
		/*       setting corresponding to default configuration from reset state. */

		/* Set ADC clock (conversion clock) */
		LL_ADC_SetClock(ADC1, LL_ADC_CLOCK_SYNC_PCLK_DIV2);

		/* Set ADC data resolution */
		// LL_ADC_SetResolution(ADC1, LL_ADC_RESOLUTION_12B);
		/* Set ADC conversion data alignment */
		// LL_ADC_SetResolution(ADC1, LL_ADC_DATA_ALIGN_RIGHT);
		/* Set ADC low power mode */
		// LL_ADC_SetLowPowerMode(ADC1, LL_ADC_LP_MODE_NONE);
		/* Set ADC channels sampling time */
		/* Note: On this STM32 serie, sampling time is common to all channels     */
		/*       of the entire ADC instance.                                      */
		/*       Therefore, sampling time is configured here under ADC instance   */
		/*       scope (not under channel scope as on some other STM32 devices    */
		/*       on which sampling time is channel wise).                         */
		/* Note: Considering interruption occurring after each ADC group          */
		/*       regular sequence conversions                                     */
		/*       (IT from DMA transfer complete),                                 */
		/*       select sampling time and ADC clock with sufficient               */
		/*       duration to not create an overhead situation in IRQHandler.      */
		LL_ADC_SetSamplingTimeCommonChannels(ADC1,
		LL_ADC_SAMPLINGTIME_160CYCLES_5);

	}

	/*## Configuration of ADC hierarchical scope: ADC group regular ############*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled or enabled without conversion on going        */
	/*       on group regular.                                                  */
	if ((LL_ADC_IsEnabled(ADC1) == 0)
			|| (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		/* Set ADC group regular trigger source */
		LL_ADC_REG_SetTriggerSource(ADC1, LL_ADC_REG_TRIG_SOFTWARE);

		/* Set ADC group regular trigger polarity */
		// LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
		/* Set ADC group regular continuous mode */
		LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_SINGLE);

		/* Set ADC group regular conversion data transfer */
		LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);

		/* Set ADC group regular overrun behavior */
		LL_ADC_REG_SetOverrun(ADC1, LL_ADC_REG_OVR_DATA_OVERWRITTEN);

		/* Set ADC group regular sequencer */
		/* Note: On this STM32 serie, ADC group regular sequencer is              */
		/*       not fully configurable: sequencer length and each rank           */
		/*       affectation to a channel are fixed by channel HW number.         */
		/*       Refer to description of function                                 */
		/*       "LL_ADC_REG_SetSequencerChannels()".                             */
		/*       Case of STM32L0xx:                                               */
		/*       ADC Channel ADC_CHANNEL_VREFINT is on ADC channel 17,            */
		/*       there is 1 other channel enabled with lower channel number.      */
		/*       Therefore, ADC_CHANNEL_VREFINT will be converted by the          */
		/*       sequencer as the 2nd rank.                                       */
		/*       ADC Channel ADC_CHANNEL_TEMPSENSOR is on ADC channel 18,         */
		/*       there are 2 other channels enabled with lower channel number.    */
		/*       Therefore, ADC_CHANNEL_TEMPSENSOR will be converted by the       */
		/*       sequencer as the 3rd rank.                                       */
		/* Set ADC group regular sequencer discontinuous mode */
		// LL_ADC_REG_SetSequencerDiscont(ADC1, LL_ADC_REG_SEQ_DISCONT_DISABLE);
		/* Set ADC group regular sequence: channel on rank corresponding to       */
		/* channel number.                                                        */
		LL_ADC_REG_SetSequencerChannels(ADC1,
		LL_ADC_CHANNEL_4 | LL_ADC_CHANNEL_VREFINT | LL_ADC_CHANNEL_TEMPSENSOR);
	}

	/*## Configuration of ADC hierarchical scope: ADC group injected ###########*/

	/* Note: Feature not available on this STM32 serie */

	/*## Configuration of ADC hierarchical scope: channels #####################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled or enabled without conversion on going        */
	/*       on either groups regular or injected.                              */
	if ((LL_ADC_IsEnabled(ADC1) == 0)
			|| (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		/* Set ADC channels sampling time */
		/* Note: On this STM32 serie, sampling time is common to all channels     */
		/*       of the entire ADC instance.                                      */
		/*       See sampling time configured above, at ADC instance scope.       */

	}

	/*## Configuration of ADC transversal scope: analog watchdog ###############*/

	/* Note: On this STM32 serie, there is only 1 analog watchdog available.    */

	/* Set ADC analog watchdog: channels to be monitored */
	// LL_ADC_SetAnalogWDMonitChannels(ADC1, LL_ADC_AWD_DISABLE);
	/* Set ADC analog watchdog: thresholds */
	// LL_ADC_ConfigAnalogWDThresholds(ADC1, __LL_ADC_DIGITAL_SCALE(LL_ADC_RESOLUTION_12B), 0x000);
	/*## Configuration of ADC transversal scope: oversampling ##################*/

	/* Set ADC oversampling scope */
	// LL_ADC_SetOverSamplingScope(ADC1, LL_ADC_OVS_DISABLE);
	/* Set ADC oversampling parameters */
	// LL_ADC_ConfigOverSamplingRatioShift(ADC1, LL_ADC_OVS_RATIO_2, LL_ADC_OVS_SHIFT_NONE);
	/*## Configuration of ADC interruptions ####################################*/
	/* Enable interruption ADC group regular end of sequence conversions */
	LL_ADC_EnableIT_EOS(ADC1);

	/* Enable interruption ADC group regular overrun */
	LL_ADC_EnableIT_OVR(ADC1);

	/* Note: in this example, ADC group regular end of conversions              */
	/*       (number of ADC conversions defined by DMA buffer size)             */
	/*       are notified by DMA transfer interruptions).                       */
	/*       ADC interruptions of end of conversion are enabled optionally,     */
	/*       as demonstration purpose in this example.                          */

	/* USER CODE END ADC_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void) {

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.Timing = 0x00100E16;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c1) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
 * @brief LPTIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPTIM1_Init(void) {

	/* USER CODE BEGIN LPTIM1_Init 0 */

	/* USER CODE END LPTIM1_Init 0 */

	/* USER CODE BEGIN LPTIM1_Init 1 */

	/* USER CODE END LPTIM1_Init 1 */
	hlptim1.Instance = LPTIM1;
	hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
	hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
	hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
	hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
	hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
	hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
	if (HAL_LPTIM_Init(&hlptim1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LPTIM1_Init 2 */

	/* USER CODE END LPTIM1_Init 2 */

}

/**
 * @brief LPUART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPUART1_UART_Init(void) {

	/* USER CODE BEGIN LPUART1_Init 0 */

	/* USER CODE END LPUART1_Init 0 */

	/* USER CODE BEGIN LPUART1_Init 1 */

	/* USER CODE END LPUART1_Init 1 */
	hlpuart1.Instance = LPUART1;
	hlpuart1.Init.BaudRate = 9600;
	hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
	hlpuart1.Init.StopBits = UART_STOPBITS_1;
	hlpuart1.Init.Parity = UART_PARITY_NONE;
	hlpuart1.Init.Mode = UART_MODE_TX_RX;
	hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&hlpuart1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LPUART1_Init 2 */

	/* USER CODE END LPUART1_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
	/**USART1 GPIO Configuration
	 PB6   ------> USART1_TX
	 PB7   ------> USART1_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART1 interrupt Init */
	NVIC_SetPriority(USART1_IRQn, 0);
	NVIC_EnableIRQ(USART1_IRQn);

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	USART_InitStruct.BaudRate = 9600;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART1, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART1);
	LL_USART_Enable(USART1);
	/* USER CODE BEGIN USART1_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART1)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART1)))) {
	}
	LL_USART_EnableIT_RXNE(USART1);
	/* USER CODE END USART1_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 9600;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
 * @brief USART4 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART4_UART_Init(void) {

	/* USER CODE BEGIN USART4_Init 0 */

	/* USER CODE END USART4_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART4);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
	/**USART4 GPIO Configuration
	 PA0   ------> USART4_TX
	 PA1   ------> USART4_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* USART4 interrupt Init */
	NVIC_SetPriority(USART4_5_IRQn, 0);
	NVIC_EnableIRQ(USART4_5_IRQn);

	/* USER CODE BEGIN USART4_Init 1 */

	/* USER CODE END USART4_Init 1 */
	USART_InitStruct.BaudRate = 115200;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART4, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART4);
	LL_USART_Enable(USART4);
	/* USER CODE BEGIN USART4_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART4)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART4)))) {
	}
	LL_USART_EnableIT_RXNE(USART4);
	/* USER CODE END USART4_Init 2 */

}

/**
 * @brief USART5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART5_UART_Init(void) {

	/* USER CODE BEGIN USART5_Init 0 */

	/* USER CODE END USART5_Init 0 */

	LL_USART_InitTypeDef USART_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART5);

	LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
	/**USART5 GPIO Configuration
	 PB3   ------> USART5_TX
	 PB4   ------> USART5_RX
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_3;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_6;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART5 interrupt Init */
	NVIC_SetPriority(USART4_5_IRQn, 0);
	NVIC_EnableIRQ(USART4_5_IRQn);

	/* USER CODE BEGIN USART5_Init 1 */

	/* USER CODE END USART5_Init 1 */
	USART_InitStruct.BaudRate = 9600;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART5, &USART_InitStruct);
	LL_USART_ConfigAsyncMode(USART5);
	LL_USART_Enable(USART5);
	/* USER CODE BEGIN USART5_Init 2 */
	/* Polling USART initialisation */
	while ((!(LL_USART_IsActiveFlag_TEACK(USART5)))
			|| (!(LL_USART_IsActiveFlag_REACK(USART5)))) {
	}
	LL_USART_EnableIT_RXNE(USART5);
	/* USER CODE END USART5_Init 2 */

}

/**
 * @brief RTC Initialization Function
 * @param None
 * @retval None
 */
static void MX_RTC_Init(void) {

	/* USER CODE BEGIN RTC_Init 0 */
	/*	 Enable Power Control clock
	 __HAL_RCC_PWR_CLK_ENABLE();

	 Enable Ultra low power mode
	 HAL_PWREx_EnableUltraLowPower();

	 Enable the fast wake up from Ultra low power mode
	 HAL_PWREx_EnableFastWakeUp();*/
	/* USER CODE END RTC_Init 0 */

	/* USER CODE BEGIN RTC_Init 1 */

	/* USER CODE END RTC_Init 1 */
	/** Initialize RTC Only
	 */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 0x7C;
	hrtc.Init.SynchPrediv = 0x0127;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the WakeUp
	 */
	if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 0x2616,
	RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN RTC_Init 2 */

	/* USER CODE END RTC_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */
	/* Compute the prescaler value to have TIMx counter clock equal to 2500 Hz */
	uwPrescalerValue = (uint32_t) (SystemCoreClock / 2500) - 1;
	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = uwPrescalerValue;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 2500 - 1;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */
	HAL_TIM_Base_Start_IT(&htim2);
	/* USER CODE END TIM2_Init 2 */

}

/** 
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE()
	;

	/* DMA interrupt init */
	/* DMA1_Channel1_IRQn interrupt configuration */
	NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;
	__HAL_RCC_GPIOH_CLK_ENABLE()
	;
	__HAL_RCC_GPIOA_CLK_ENABLE()
	;
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPRS_RST_Pin | WISOL_WKP_Pin | WISOL_RST_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA,
			GPIO_LED_Pin | EN_US_Pin | EN_GPRS_Pin | EN_GPS_Pin | EN_BLE_Pin
					| EN_SFOX_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPRS_PWR_ON_GPIO_Port, GPRS_PWR_ON_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : WISOL_LED_CPU_Pin */
	GPIO_InitStruct.Pin = WISOL_LED_CPU_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(WISOL_LED_CPU_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PC14 PC15 */
	GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : PH0 PH1 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	/*Configure GPIO pin : PA5 */
	GPIO_InitStruct.Pin = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : HC_SR04_TRG_Pin HC_SR04_PULSE_Pin */
	GPIO_InitStruct.Pin = HC_SR04_TRG_Pin | HC_SR04_PULSE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB0 PB1 PB12 PB13 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_12 | GPIO_PIN_13;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : GPRS_RST_Pin */
	GPIO_InitStruct.Pin = GPRS_RST_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPRS_RST_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : WISOL_WKP_Pin WISOL_RST_Pin GPRS_PWR_ON_Pin */
	GPIO_InitStruct.Pin = WISOL_WKP_Pin | WISOL_RST_Pin | GPRS_PWR_ON_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : GPIO_LED_Pin */
	GPIO_InitStruct.Pin = GPIO_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIO_LED_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : EN_US_Pin EN_GPRS_Pin EN_GPS_Pin EN_BLE_Pin
	 EN_SFOX_Pin */
	GPIO_InitStruct.Pin = EN_US_Pin | EN_GPRS_Pin | EN_GPS_Pin | EN_BLE_Pin
			| EN_SFOX_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

//*********************** ADC FUNCTIONS ***************************
void fn_call_adc_conversion() {
	Activate_ADC();
	if (ubDmaTransferStatus != 0) {
		ubDmaTransferStatus = 0;
	}
	if ((LL_ADC_IsEnabled(ADC1) == 1) && (LL_ADC_IsDisableOngoing(ADC1) == 0)
			&& (LL_ADC_REG_IsConversionOngoing(ADC1) == 0)) {
		LL_ADC_REG_StartConversion(ADC1);
	}
	/* Note: ADC group regular conversion start is done into push button      */
	/*       IRQ handler, refer to function "UserButton_Callback()".          */

	/* Note: LED state depending on DMA transfer status is set into DMA       */
	/*       IRQ handler, refer to function "DmaTransferComplete()".          */

	/* Note: For this example purpose, number of ADC group regular sequences  */
	/*       completed are stored into variable                               */
	/*       "ubAdcGrpRegularSequenceConvCount"                               */
	/*       (for debug: see variable content into watch window)              */

	/* Note: ADC conversions data are stored into array "aADCxConvertedData"  */
	/*       (for debug: see variable content into watch window).             */
	/*       Each rank of the sequence is at an address of the array:         */
	/*       - aADCxConvertedData[0]: ADC channel set on rank1                */
	/*                                (ADC1 channel 4)                      */
	/*       - aADCxConvertedData[1]: ADC channel set on rank2                */
	/*                                (ADC1 internal channel VrefInt)             */
	/*       - aADCxConvertedData[2]: ADC channel set on rank3                */
	/*                                (ADC1 internal channel temperature sensor)*/

	/* Wait for ADC conversion and DMA transfer completion to process data */

	/* Computation of ADC conversions raw data to physical values             */
	/* using LL ADC driver helper macro.                                      */
	uhADCxConvertedData_VoltageGPIO_mVolt = __LL_ADC_CALC_DATA_TO_VOLTAGE(
			VDDA_APPLI, aADCxConvertedData[0], LL_ADC_RESOLUTION_12B);
	uhADCxConvertedData_VrefInt_mVolt = __LL_ADC_CALC_DATA_TO_VOLTAGE(
			VDDA_APPLI, aADCxConvertedData[1], LL_ADC_RESOLUTION_12B);
	hADCxConvertedData_Temperature_DegreeCelsius = __LL_ADC_CALC_TEMPERATURE(
			VDDA_APPLI, aADCxConvertedData[2], LL_ADC_RESOLUTION_12B);

	/* Optionally, for this example purpose, calculate analog reference       */
	/* voltage (Vref+) from ADC conversion of internal voltage reference      */
	/* VrefInt.                                                               */
	/* This voltage should correspond to value of literal "VDDA_APPLI".       */
	/* Note: This calculation can be performed when value of voltage Vref+    */
	/* is unknown in the application.                                         */
	uhADCxConvertedData_VrefAnalog_mVolt = __LL_ADC_CALC_VREFANALOG_VOLTAGE(
			aADCxConvertedData[1], LL_ADC_RESOLUTION_12B);

	/* Wait for a new ADC conversion and DMA transfer */

}

/**
 * @brief  Perform ADC activation procedure to make it ready to convert
 *         (ADC instance: ADC1).
 * @note   Operations:
 *         - ADC instance
 *           - Run ADC self calibration
 *           - Enable ADC
 *         - ADC group regular
 *           none: ADC conversion start-stop to be performed
 *                 after this function
 *         - ADC group injected
 *           Feature not available                                  (feature not available on this STM32 serie)
 * @param  None
 * @retval None
 */

void Activate_ADC(void) {
	__IO uint32_t wait_loop_index = 0;
	__IO uint32_t backup_setting_adc_dma_transfer = 0;
#if (USE_TIMEOUT == 1)
	uint32_t Timeout = 0; /* Variable used for timeout management */
#endif /* USE_TIMEOUT */

	/*## Operation on ADC hierarchical scope: ADC instance #####################*/

	/* Note: Hardware constraint (refer to description of the functions         */
	/*       below):                                                            */
	/*       On this STM32 serie, setting of these features is conditioned to   */
	/*       ADC state:                                                         */
	/*       ADC must be disabled.                                              */
	/* Note: In this example, all these checks are not necessary but are        */
	/*       implemented anyway to show the best practice usages                */
	/*       corresponding to reference manual procedure.                       */
	/*       Software can be optimized by removing some of these checks, if     */
	/*       they are not relevant considering previous settings and actions    */
	/*       in user application.                                               */
	if (LL_ADC_IsEnabled(ADC1) == 0) {
		/* Disable ADC DMA transfer request during calibration */
		/* Note: Specificity of this STM32 serie: Calibration factor is           */
		/*       available in data register and also transfered by DMA.           */
		/*       To not insert ADC calibration factor among ADC conversion data   */
		/*       in array variable, DMA transfer must be disabled during          */
		/*       calibration.                                                     */
		backup_setting_adc_dma_transfer = LL_ADC_REG_GetDMATransfer(ADC1);
		LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_NONE);

		/* Run ADC self calibration */
		LL_ADC_StartCalibration(ADC1);

		/* Poll for ADC effectively calibrated */
#if (USE_TIMEOUT == 1)
		Timeout = ADC_CALIBRATION_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

		while (LL_ADC_IsCalibrationOnGoing(ADC1) != 0) {
#if (USE_TIMEOUT == 1)
			/* Check Systick counter flag to decrement the time-out value */
			if (LL_SYSTICK_IsActiveCounterFlag())
			{
				if(Timeout-- == 0)
				{
					/* Time-out occurred. Set LED to blinking mode */
					LED_Blinking(LED_BLINK_ERROR);
				}
			}
#endif /* USE_TIMEOUT */
		}

		/* Delay between ADC end of calibration and ADC enable.                   */
		/* Note: Variable divided by 2 to compensate partially                    */
		/*       CPU processing cycles (depends on compilation optimization).     */
		wait_loop_index = (ADC_DELAY_CALIB_ENABLE_CPU_CYCLES >> 1);
		while (wait_loop_index != 0) {
			wait_loop_index--;
		}

		/* Restore ADC DMA transfer request after calibration */
		LL_ADC_REG_SetDMATransfer(ADC1, backup_setting_adc_dma_transfer);

		/* Enable ADC */
		LL_ADC_Enable(ADC1);

		/* Poll for ADC ready to convert */
#if (USE_TIMEOUT == 1)
		Timeout = ADC_ENABLE_TIMEOUT_MS;
#endif /* USE_TIMEOUT */

		while (LL_ADC_IsActiveFlag_ADRDY(ADC1) == 0) {
#if (USE_TIMEOUT == 1)
			/* Check Systick counter flag to decrement the time-out value */
			if (LL_SYSTICK_IsActiveCounterFlag())
			{
				if(Timeout-- == 0)
				{
					/* Time-out occurred. Set LED to blinking mode */
					LED_Blinking(LED_BLINK_ERROR);
				}
			}
#endif /* USE_TIMEOUT */
		}

		/* Note: ADC flag ADRDY is not cleared here to be able to check ADC       */
		/*       status afterwards.                                               */
		/*       This flag should be cleared at ADC Deactivation, before a new    */
		/*       ADC activation, using function "LL_ADC_ClearFlag_ADRDY()".       */
	}

	/*## Operation on ADC hierarchical scope: ADC group regular ################*/
	/* Note: No operation on ADC group regular performed here.                  */
	/*       ADC group regular conversions to be performed after this function  */
	/*       using function:                                                    */
	/*       "LL_ADC_REG_StartConversion();"                                    */

	/*## Operation on ADC hierarchical scope: ADC group injected ###############*/
	/* Note: Feature not available on this STM32 serie */

}

/******************************************************************************/
/*   USER IRQ HANDLER TREATMENT                                               */
/******************************************************************************/

/**
 * @brief  DMA transfer complete callback
 * @note   This function is executed when the transfer complete interrupt
 *         is generated
 * @retval None
 */
void AdcDmaTransferComplete_Callback() {
	/* Update status variable of DMA transfer */
	ubDmaTransferStatus = 1;

	/* Set LED depending on DMA transfer status */
	/* - Turn-on if DMA transfer is completed */
	/* - Turn-off if DMA transfer is not completed */

	/* For this example purpose, check that DMA transfer status is matching     */
	/* ADC group regular sequence status:                                       */
	if (ubAdcGrpRegularSequenceConvStatus != 1) {
		AdcDmaTransferError_Callback();
	}

	/* Reset status variable of ADC group regular sequence */
	ubAdcGrpRegularSequenceConvStatus = 0;
}

/**
 * @brief  DMA transfer error callback
 * @note   This function is executed when the transfer error interrupt
 *         is generated during DMA transfer
 * @retval None
 */
void AdcDmaTransferError_Callback() {

}

/**
 * @brief  ADC group regular end of sequence conversions interruption callback
 * @note   This function is executed when the ADC group regular
 *         sequencer has converted all ranks of the sequence.
 * @retval None
 */
void AdcGrpRegularSequenceConvComplete_Callback() {
	/* Update status variable of ADC group regular sequence */
	ubAdcGrpRegularSequenceConvStatus = 1;
	ubAdcGrpRegularSequenceConvCount++;
}

/**
 * @brief  ADC group regular overrun interruption callback
 * @note   This function is executed when ADC group regular
 *         overrun error occurs.
 * @retval None
 */
void AdcGrpRegularOverrunError_Callback(void) {
	/* Note: Disable ADC interruption that caused this error before entering in */
	/*       infinite loop below.                                               */

	/* Disable ADC group regular overrun interruption */
	LL_ADC_DisableIT_OVR(ADC1);

	/* Error from ADC */

}

//************** STM INTERNAL VALUES FUNCTIONS **********************************

void fn_get_stm32_temperature() {
	fn_call_adc_conversion();
	if (hADCxConvertedData_Temperature_DegreeCelsius < 0) {
		st_stm_adc_variables.temperature =
				hADCxConvertedData_Temperature_DegreeCelsius * (-1);
	} else {
		st_stm_adc_variables.temperature =
				hADCxConvertedData_Temperature_DegreeCelsius;
	}
	if (st_stm_adc_variables.temperature >= 100)
		st_stm_adc_variables.temperature /= 10;
}

void fn_get_stm32_volts() {
	fn_call_adc_conversion();
	st_stm_adc_variables.battery = uhADCxConvertedData_VoltageGPIO_mVolt
			* (1.89 / 1.5);
}

void fn_change_baundrate(int Baud_rate) {
	HAL_Delay(200);
	huart4.Instance = USART4;
	huart4.Init.BaudRate = Baud_rate;
	huart4.Init.WordLength = UART_WORDLENGTH_8B;
	huart4.Init.StopBits = UART_STOPBITS_1;
	huart4.Init.Parity = UART_PARITY_NONE;
	huart4.Init.Mode = UART_MODE_TX_RX;
	huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart4.Init.OverSampling = UART_OVERSAMPLING_16;
	huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart4) != HAL_OK) {
		Error_Handler();
	}
	HAL_Delay(1000);
}

void blink(uint8_t times) {
	for (int var = 0; var < times; ++var) {
		LED_ON
		HAL_Delay(25);
		LED_OFF
		HAL_Delay(25);
	}

}

//************************************** PRINT FUCTIONS ****************************

void fn_fprintnumber(unsigned int number) {
	char numbuff[] = "\0";
	utoa(number, numbuff, 10);
	fn_fprint(numbuff);
}

void fn_fprint(char *data) {
	int tamanho = strlen(data);
	char new_command[tamanho];
	strcpy(new_command, data);
	char new_com[1]; // (uint8_t*)new_command;
	for (int var = 0; var < tamanho; ++var) {
		new_com[0] = new_command[var];
		HAL_UART_Transmit(&hlpuart1, (uint8_t*) new_com, 1, 20);
	}
}

void fn_print_sensor_values() {
	char angleBUFF[1] = { 0 };
	char distanceBUFF[3] = { 0 };
	char latitudeBUFF[7] = { 0 };
	char longitiudeBUFF[7] = { 0 };
	char batteryBUFF[3] = { 0 };
	char temperatureBUFF[2] = { 0 };

	HAL_Delay(1000);
	fn_fprint("\r\n");
	fn_fprint("********* SENSORS VALUES ***********");
	fn_fprint("\r\n");
	fn_fprint("ANGLE: ");
	utoa(st_data_sensor_e.angle, angleBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) angleBUFF, 1, 100);
	HAL_Delay(50);
	fn_fprint(" graus\r\n");
	fn_fprint("DISTANCE: ");
	utoa(st_data_sensor_e.distance, distanceBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) distanceBUFF, 3, 100);
	HAL_Delay(50);
	fn_fprint(" cm\r\n");
	fn_fprint("BLE DISTANCE: ");
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) ble_distance, 2, 100);
	HAL_Delay(50);
	fn_fprint(" [hex]\r\n");
	fn_fprint("TEMPERATURE: ");
	utoa(st_data_sensor_e.temperature, temperatureBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) temperatureBUFF, 2, 100);
	HAL_Delay(50);
	fn_fprint(" Celsius\r\n");
	fn_fprint("BATTERY: ");
	utoa((st_data_sensor_e.battery + 120), batteryBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) batteryBUFF, 3, 100);
	HAL_Delay(50);
	fn_fprint(" volts\r\n");
	fn_fprint("LATITUDE: ");
	utoa(st_data_sensor_e.latitude, latitudeBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) latitudeBUFF, 7, 100);
	HAL_Delay(50);
	fn_fprint("\r\n");
	fn_fprint("LONGITUDE: ");
	utoa(st_data_sensor_e.longitude, longitiudeBUFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) longitiudeBUFF, 7, 100);
	HAL_Delay(50);
	fn_fprint("\r\n");

	fn_fprint("\r\n########################################\r\n");

}

void fn_print_timers_values() {
	char timercounterBFF[5] = { 0 };
	//char keepAliveBFF[5] = { 0 };

	fn_fprint("\r\ntimer counter: ");
	utoa(TimeCounter, timercounterBFF, 10);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) timercounterBFF, 5, 100);
	HAL_Delay(50);

	/*fn_fprint("\r\nkeep alive enter: ");
	 utoa(enterKeepAliveTimer, keepAliveBFF, 10);
	 HAL_UART_Transmit(&hlpuart1, (uint8_t*) keepAliveBFF, 5, 100);
	 HAL_Delay(50);*/
}

void fn_print_lsm_values() {
	char axBFF[10] = { 0 };
	char ayBFF[10] = { 0 };
	char azBFF[10] = { 0 };
	char mxBFF[10] = { 0 };
	char myBFF[10] = { 0 };
	char mzBFF[10] = { 0 };

	struct {
		int16_t a_x, a_y, a_z;
	} st_ac_buffer;

	struct {
		int16_t m_x, m_y, m_z;
	} st_mag_buffer;

	if (st_accelerometer.a_x < 0)
		st_ac_buffer.a_x = (st_accelerometer.a_x * -1);
	if (st_accelerometer.a_y < 0)
		st_ac_buffer.a_y = (st_accelerometer.a_y * -1);
	if (st_accelerometer.a_z < 0)
		st_ac_buffer.a_z = (st_accelerometer.a_z * -1);
	if (st_magnetometer.m_x < 0)
		st_mag_buffer.m_x = (st_magnetometer.m_x * -1);
	if (st_magnetometer.m_y < 0)
		st_mag_buffer.m_y = (st_magnetometer.m_y * -1);
	if (st_magnetometer.m_z < 0)
		st_mag_buffer.m_z = (st_magnetometer.m_z * -1);

	if (st_accelerometer.a_x >= 0)
		st_ac_buffer.a_x = (st_accelerometer.a_x * 1);
	if (st_accelerometer.a_y >= 0)
		st_ac_buffer.a_y = (st_accelerometer.a_y * 1);
	if (st_accelerometer.a_z >= 0)
		st_ac_buffer.a_z = (st_accelerometer.a_z * 1);
	if (st_magnetometer.m_x >= 0)
		st_mag_buffer.m_x = (st_magnetometer.m_x * 1);
	if (st_magnetometer.m_y >= 0)
		st_mag_buffer.m_y = (st_magnetometer.m_y * 1);
	if (st_magnetometer.m_z >= 0)
		st_mag_buffer.m_z = (st_magnetometer.m_z * 1);

	utoa(st_ac_buffer.a_x, axBFF, 10);
	utoa(st_ac_buffer.a_y, ayBFF, 10);
	utoa(st_ac_buffer.a_z, azBFF, 10);
	utoa(st_mag_buffer.m_x, mxBFF, 10);
	utoa(st_mag_buffer.m_y, myBFF, 10);
	utoa(st_mag_buffer.m_z, mzBFF, 10);

	if (st_accelerometer.a_x < 0) {
		fn_fprint("\r\nax: -");
	} else {
		fn_fprint("\r\nax: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) axBFF, 10, 100);
	HAL_Delay(50);
	if (st_accelerometer.a_y < 0) {
		fn_fprint("\r\nay: -");
	} else {
		fn_fprint("\r\nay: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) ayBFF, 10, 100);
	HAL_Delay(50);

	if (st_accelerometer.a_z < 0) {
		fn_fprint("\r\naz: -");
	} else {
		fn_fprint("\r\naz: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) azBFF, 10, 100);
	HAL_Delay(50);

	if (st_magnetometer.m_x < 0) {
		fn_fprint("\r\nmx: -");
	} else {
		fn_fprint("\r\nmx: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) mxBFF, 10, 100);
	HAL_Delay(50);
	if (st_magnetometer.m_y < 0) {
		fn_fprint("\r\nmy: -");
	} else {
		fn_fprint("\r\nmy: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) myBFF, 10, 100);
	HAL_Delay(50);
	if (st_magnetometer.m_z < 0) {
		fn_fprint("\r\nmz: -");
	} else {
		fn_fprint("\r\nmz: ");
	}
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) mzBFF, 10, 100);
	HAL_Delay(50);
}

// **************************** CALLBACK FUNCTIONS **************************************

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	UartReady = SET;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	//LED_CHANGE
	TimeCounter++;

}

/**
 * @brief  RTC Wake Up callback
 * @param  None
 * @retval None
 */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc) {
	/* Clear Wake Up Flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

}

//*********************** AUXILIAR FUNCTIONS ***************************

void fn_check_communication_status_program() {
	fn_fprint("\r\n_________STATUS COMMUNICATION MODULES___________\r\n");
	fn_fprint("\r\nCHECKING...");

	st_flag.BLE = fn_ckeck_Nina_status();
	st_flag.SigFox = fn_check_sigfox_status();
	st_flag.GPRS = fn_check_sarag450_status();

	if (st_flag.SigFox == 1)
		fn_fprint("\r\n IOT - find");
	if (st_flag.GPRS == 1)
		fn_fprint("\r\nGPRS - find");
	if (st_flag.BLE == 1)
		fn_fprint("\r\n BLE - find");

	if (st_flag.SigFox == 0 || st_flag.GPRS == 0 || st_flag.BLE == 0) {
		fn_fprint("\r\nRECHECKING...");

		if (st_flag.BLE == 0)
			st_flag.BLE = fn_ckeck_Nina_status();

		if (st_flag.SigFox == 0)
			st_flag.SigFox = fn_check_sigfox_status();

		if (st_flag.GPRS == 0)
			st_flag.GPRS = fn_check_sarag450_status();

		if (st_flag.SigFox == 1)
			fn_fprint("\r\n IOT - find");
		if (st_flag.GPRS == 1)
			fn_fprint("\r\nGPRS - find");
		if (st_flag.BLE == 1)
			fn_fprint("\r\n BLE - find");
	}
	fn_fprint("\r\n*******************END****************\r\n");

	if (st_flag.SigFox == 1)
		fn_info_sigfox();
	fn_fprint("\r\n***********************************\r\n");

}

void fn_check_sensors_status_program() {
	fn_fprint("\r\n_________STATUS SENSORS MODULES___________\r\n");
	fn_fprint("\r\nCHECKING...");

	st_flag.GPS = fn_check_camM8_status();
	st_flag.acelerometer = fn_init_lsm303ah();
	st_flag.ultrassonic = fn_check_sen031x_status();

	if (st_flag.ultrassonic == 1)
		fn_fprint("\r\nSEN ULTRSSONIC - find");
	if (st_flag.GPS == 1)
		fn_fprint("\r\nGPS CAM M8 - find");
	if (st_flag.acelerometer == 1)
		fn_fprint("\r\nLSM303AHR - find");

	if (st_flag.GPS == 0 || st_flag.acelerometer == 0
			|| st_flag.ultrassonic == 0) {
		fn_fprint("\r\nRECHECKING...");

		if (st_flag.GPS == 0)
			st_flag.GPS = fn_check_camM8_status();
		if (st_flag.acelerometer == 0)
			st_flag.acelerometer = fn_init_lsm303ah();
		if (st_flag.ultrassonic == 0)
			st_flag.ultrassonic = fn_check_sen031x_status();

		if (st_flag.ultrassonic == 1)
			fn_fprint("\r\nSEN ULTRSSONIC - find");
		if (st_flag.GPS == 1)
			fn_fprint("\r\nGPS CAM M8 - find");
		if (st_flag.acelerometer == 1)
			fn_fprint("\r\nLSM303AHR - find");
	}
	fn_fprint("\r\n*****************END*********************\r\n");
}

void fn_send_payload(uint8_t downlink_frame_type) {
	switch (downlink_frame_type) {
	case INFO_FRAME_PROCESSED:
		if (st_flag.LoRa)
			fn_send_report_frame_lora();
		if (st_flag.SigFox)
			fn_send_report_frame_sigfox();
		if (st_flag.GPRS) {
			fn_power_on_sara();
			fn_start_at_commands_sara();
			fn_at_command_saraG450("AT+UFACTORY=1,1", 100);
			fn_start_at_commands_sara();
			fn_get_module_info_sara();
			fn_network_activation_sara();
			fn_http_mensage_sara(INFO_FRAME_PROCESSED);

			fn_power_off_sara();
		}
		break;
	case DAILY_UPDATE_FRAME:
		if (st_flag.SigFox)
			fn_send_daily_frame_sigfox();
		if (st_flag.GPRS) {
			fn_power_on_sara();
			fn_start_at_commands_sara();
			fn_at_command_saraG450("AT+UFACTORY=1,1", 100);
			fn_start_at_commands_sara();
			fn_get_module_info_sara();
			if (fn_network_activation_sara() == 1) {
				fn_http_mensage_sara(DAILY_UPDATE_FRAME);
			}
			fn_power_off_sara();
		}

		break;
	case CONFIG_FRAME:
		break;
	case START_FRAME:
		if (st_flag.SigFox)
			fn_send_start_frame_sigfox();
		if (st_flag.GPRS) {

		}
		break;
	case ALERT_FRAME:
		if (st_flag.SigFox)
			fn_send_report_frame_sigfox();
		if (st_flag.GPRS) {
			fn_power_on_sara();
			fn_start_at_commands_sara();
			fn_at_command_saraG450("AT+UFACTORY=1,1", 100);
			fn_start_at_commands_sara();
			fn_get_module_info_sara();
			if (fn_network_activation_sara() == 1) {
				fn_http_mensage_sara(ALERT_FRAME);
			}
			fn_power_off_sara();
		}
		break;
	case TAGO_FRAME:
		if (st_flag.LoRa)
			fn_send_report_frame_tago();
		if (st_flag.SigFox)
			fn_send_report_frame_sigfox();
		if (st_flag.GPRS) {
			fn_power_on_sara();
			fn_start_at_commands_sara();
			fn_at_command_saraG450("AT+UFACTORY=1,1", 100);
			fn_start_at_commands_sara();
			fn_get_module_info_sara();
			fn_network_activation_sara();
			fn_http_mensage_sara(TAGO_FRAME);

			fn_power_off_sara();
		}
		break;
	default:
		break;
	}

}

//************************* LOW POWER MODE FUNCTIONS *****************

void fn_enter_lowPowerMode() {
	fn_fprint("\r\nENTER LPM\r\n");

	fn_desable_GPIOs();
}

void fn_exit_lowPowerMode() {
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	HAL_I2C_MspInit(&hi2c1);
	MX_I2C1_Init();
	HAL_UART_MspInit(&hlpuart1);
	MX_LPUART1_UART_Init();
//	HAL_UART_MspInit(&huart5);
	MX_USART1_UART_Init();
	HAL_UART_MspInit(&huart2);
	MX_USART2_UART_Init();

	MX_USART4_UART_Init();
	MX_USART5_UART_Init();
}

void fn_desable_GPIOs() {
	// GPIO_A is preserved to keep output status unchanged and have
	// Interrupt working for waking Up.

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE()
	;
	__HAL_RCC_GPIOA_CLK_ENABLE()
	;
	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	GPIO_InitStruct.Pin = GPIO_PIN_All;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;

	// GPIO_B and GPIO_C are disabled
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	__HAL_RCC_GPIOA_CLK_DISABLE();
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	__HAL_RCC_GPIOB_CLK_DISABLE();
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	__HAL_RCC_GPIOC_CLK_DISABLE();
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line) {
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
