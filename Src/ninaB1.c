/*
 * ninaB1.c
 *
 *  Created on: 6 de abr de 2020
 *      Author: oscar
 */

#include "ninaB1.h"
#include "stm32l0xx_it.h"
char teste[5] = { 0 };

__IO uint8_t ubSend = 0;
uint8_t aStringToSend[100];
uint8_t ubSizeToSend = 0; //sizeof(aStringToSend);
uint8_t aStringToReceiver[BYTES_NINA_TO_RECEIVER];
uint8_t ubSizeToReceiver = 0;

//******************** MAIN BLE **************************

void fn_Change_Nina_Config_Type(e_nina_config_type nina_state) {
	e_Previous_nina_config_type = e_Current_nina_config_type;
	e_Current_nina_config_type = nina_state;
}

void fn_main_nina(void) {

	switch (e_Current_nina_config_type) {
	case INIT_MODE:

		fn_fprint("BLE_INIT_MODE\r\n");
		fn_turn_on_nina();
		flag_default_peer = RESET;
		if (!fn_at_command_attention())
			fn_at_command_escape_at();

		fn_at_command_get_serial_number();
		fn_at_command_get_model_id();
		fn_at_command_get_manufacture_id();
		fn_Change_Nina_Config_Type(EDDYSTONE_BEACON_MODE);
		break;
	case RESET_MODE:
		fn_fprint("BLE RESET_MODE\r\n");
		break;
	case FACTORY_MODE:
		fn_fprint("BLE FACTORY_MODE\r\n");
		while (!fn_at_command_attention())
			fn_at_command_escape_at();
		fn_at_command_factory_reset();
		HAL_Delay(500);
		fn_at_command_reboot_device();
		HAL_Delay(200);
		fn_Change_Nina_Config_Type(INIT_MODE);
		break;
	case CENTRAL_MODE:
		fn_turn_on_nina();
		flag_centra_config = SET;
		fn_fprint("BLE CENTRAL_MODE\r\n");
		while (!fn_at_command_attention())
			fn_at_command_escape_at();

		fn_at_command_set_new_name("WizeBox_c");
		fn_at_command_set_connectable();
		fn_at_command_set_discoverable();
		fn_at_command_set_pairable();
		fn_at_command_set_central();
		fn_at_command_storing();
		fn_at_command_reboot_device();

		fn_fprint("FIND EDDYSTONE TLM...\r\n");
		while (!fn_discovery_devices_nina()) {
			HAL_Delay(1000);
			LED_CHANGE
		}
		if (fn_at_connect_peer()) {
			while (!fn_at_command_data_mode()) {
			}
			while (flag_centra_config) {
				if (fn_data_at_command()) {
					flag_centra_config = RESET;
					break;
				}
				if (fn_data_get_at_battery()) {
					flag_centra_config = RESET;
					break;
				}
				if (fn_data_get_at_temperature()) {
					flag_centra_config = RESET;
					break;
				}
				if (fn_data_get_at_distance()) {
					flag_centra_config = RESET;
					break;
				}
			}
			StartTransfers("\r\nBACK TO AT IN\r\n");
			StartTransfers("3\r\n");
			HAL_Delay(1000);
			StartTransfers("2\r\n");
			HAL_Delay(1000);
			StartTransfers("1\r\n");
			HAL_Delay(1000);
			StartTransfers("NOW\r\n");
			if (fn_at_command_escape_at()) {
				fn_at_disconect_peer();
				break;
			}
		}
		break;
	case PERIMETRAL_MODE:
		fn_fprint("BLE PERIMETRAL_MODE\r\n");
		fn_get_sensors_char_values();
		while (!fn_at_command_attention())
			fn_at_command_escape_at();
		fn_at_command_set_new_name("WizeBox_p");
		fn_at_command_set_connectable();
		//fn_at_command_set_server_config();
		fn_at_command_set_discoverable();
		fn_at_command_set_pairable();
		fn_at_command_set_perimetral();
		fn_at_command_storing();
		fn_at_command_reboot_device();

		fn_fprint("WAITING TO CONNECT\r\n");
		while (!fn_wait_for_connection()) {
		}
		fn_at_command_data_mode();
		while (!fn_answer_at_data_mode_nina()) {
		}
		break;
	case CONFIG_MODE:
		fn_fprint("BLE CONFIG_MODE\r\n");
		fn_at_command_set_perimetral();
		break;
	case IBEACON_MODE:
		fn_turn_on_nina();
		fn_fprint("BLE IBEACON_MODE\r\n");
		if (!fn_at_command_attention())
			fn_at_command_escape_at();
		fn_at_command_set_perimetral();
		fn_at_command_set_wbeacon_name();
		fn_at_command_set_beacon_payload();
		break;
	case EDDYSTONE_BEACON_MODE:
		fn_turn_on_nina();
		fn_fprint("BLE EDDYSTONE_BEACON_MODE\r\n");
		if (!fn_at_command_attention())
			fn_at_command_escape_at();
		fn_at_command_set_perimetral();
		fn_at_command_set_wbeacon_name();
		fn_at_command_set_eddyston_TLM_payload();
		break;
	case SCAN_BEACON_MODE:
		fn_turn_on_nina();
		fn_fprint("BLE SCAN_BEACON_MODE\r\n");
		if (!fn_at_command_attention())
			fn_at_command_escape_at();
		fn_at_command_set_new_name("wBox");
		fn_at_command_set_connectable();
		fn_at_command_set_discoverable();
		fn_at_command_set_pairable();
		fn_at_command_set_central();
		//fn_at_command_storing();
		//fn_at_command_reboot_device();
		if (fn_at_command_scan_peer() == 0) {
			if (fn_at_command_scan_peer() == 0) {
				if (fn_at_command_scan_peer() == 0) {
					if (fn_at_command_scan_peer() == 0) {
						if (fn_at_command_scan_peer() == 0) {
						}
					}
				}
			}
		}
		fn_turn_off_nina();
		break;
	default:
		break;
	}

}

uint8_t fn_ckeck_Nina_status() {
	uint8_t check = 0;
	fn_turn_on_nina();
	for (int var = 0; var < BYTES_NINA_TO_RECEIVER; ++var) {
		if (aStringToReceiver[var] == 43) {
			check = 1;
			break;
		}
	}
	fn_at_command_get_serial_number();
	fn_turn_off_nina();
	return check;
}

//-----------------POWER MODE OPERATION-------------------
void fn_turn_on_nina(void) {
	BLE_ON
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
}

void fn_turn_off_nina(void) {
	HAL_Delay(1000);
	BLE_OFF

}

void fn_reset_nina(void) {
	fn_turn_off_nina();
	fn_turn_on_nina();
}

//*************************************************************

void fn_ninaB1() {
	fn_send_at_command_nina("AT", 1000);
	fn_send_at_command_nina("AT+UBTLE=1", 1000);
	fn_send_at_command_nina("AT&W", 1000);
	fn_send_at_command_nina("AT+UBTLN=\"WIZE TEST CENTRAL BLE\"", 1000);
	fn_send_at_command_nina("AT+UBTD", 5000);
	fn_send_at_command_nina("AT+UDCP=sps://CCF9578580A3p", 2000);
	HAL_Delay(1000);
	fn_send_at_command_nina("ATO1", 1000);
	HAL_Delay(1000);
	fn_send_at_command_nina("teste", 1000);

	BLE_OFF
	LED_OFF
}

uint8_t fn_set_config_central_nina() {
	//fn_set_at_mode_nina();
	//fn_send_at_command_nina("AT", 1000);
	//fn_send_at_command_nina("AT+UBTLE=1", 1000);
	//fn_send_at_command_nina("AT&W", 1000);
	//fn_send_at_command_nina("AT+CPWROFF", 1000);
	HAL_Delay(1000);
	//fn_send_at_command_nina("AT+UDSC=0,6", 1000);
	//HAL_Delay(500);

	return 1;
}

//##############  CONFIG DATA MODE  #####################
uint8_t fn_at_command_escape_at() {
	uint8_t atReturn = 0;
	LED_ON
	HAL_Delay(1500);
	StartTransfers("+++");
	HAL_Delay(1500);
	LED_OFF
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_data_mode() {

	uint8_t atReturn = 0;
	StartTransfers("ATO1\r");
	HAL_Delay(200);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}
//*************************************************************

/*


 int fn_discovery_peer_nina() {

 memset(responce_nina_buff, 0, BYTES_NINA_TO_RECEIVER);

 HAL_UART_Transmit_DMA(&huart4, (uint8_t*) "AT+UBTD\r\n", 9);
 while (UartReady != SET) {
 }
 UartReady = RESET;
 HAL_UART_Receive_DMA(&huart4, (uint8_t*) responce_nina_buff, BYTES_NINA_TO_RECEIVER);
 while (UartReady != SET) {

 }
 UartReady = RESET;

 memset(responce_nina_buff, 0, BYTES_NINA_TO_RECEIVER);
 int return_flag = 0;

 char BLEreceiver[BYTES_NINA_TO_RECEIVER] = { 0 };

 HAL_UART_Transmit_IT(&huart4, (uint8_t*) "AT+UBTD\r\n", 9);
 while (UartReady != SET) {
 }
 UartReady = RESET;

 HAL_Delay(5000);

 HAL_UART_Receive_IT(&huart4, (uint8_t*) &BLEreceiver,
 BYTES_NINA_TO_RECEIVER);
 LED_ON
 HAL_Delay(10);
 LED_OFF
 huart4.pRxBuffPtr = (uint8_t*) &BLEreceiver;
 huart4.RxXferCount = 0;

 int size_response_uart = 0;

 for (int var = 0; var < BYTES_NINA_TO_RECEIVER; var++) {
 if (BLEreceiver[var] != 0 && BLEreceiver[var] < 128) {
 ++size_response_uart;
 }
 }
 //char sendResponse[size_response_uart];
 int counter_size = 0;

 for (int var = 0; var < BYTES_NINA_TO_RECEIVER; var++) {
 if (BLEreceiver[var] != 0 && BLEreceiver[var] < 128) {
 //sendResponse[counter_size] = BLEreceiver[var];
 responce_nina_buff[counter_size] = BLEreceiver[var];
 ++counter_size;
 if (responce_nina_buff[counter_size - 1] == 79
 && responce_nina_buff[counter_size] == 75) {
 return_flag = 1;
 }
 }
 }

 fn_fprint(responce_nina_buff);

 return return_flag;

 }
 */

//##############  AT COMMANDS  #####################
uint8_t fn_at_command_attention() {
	uint8_t atReturn = 0;
	StartTransfers("AT\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_storing() {
	uint8_t atReturn = 0;
	StartTransfers("AT&W\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_factory_reset() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UFACTORY\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_reboot_device() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CPWROFF\r");
	HAL_Delay(2000);
	fn_at_uart_reception_nina();

	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	HAL_Delay(500);
	fn_at_uart_reception_nina();
	return atReturn;
}

uint8_t fn_at_command_set_perimetral() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTLE=2\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_central() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTLE=1\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_serial_number() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGSN\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_model_id() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGMM\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_get_manufacture_id() {
	uint8_t atReturn = 0;
	StartTransfers("AT+CGMI\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_new_name(char*name) {

	int size_command = strlen(name);
	size_command += 10;
	char command[size_command];
	strcpy(command, "AT+UBTLN=");
	strcat(command, name);
	strcat(command, "\r");
	uint8_t atReturn = 0;
	StartTransfers(command);
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_server_config() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UDSC=0,6\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_connectable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTCM=2\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_discoverable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTDM=3\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_pairable() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTPM=2\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_wbeacon_name() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UBTSD=080977426561636F6E\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_set_eddyston_TLM_payload() {
	uint8_t atReturn = 0;
	char payload_ble_eddystone[44] = { 0 };
	char command_complete_ble_eddystone[54] = { 0 };
	fn_encoder_ble_values(payload_ble_eddystone);
	command_complete_ble_eddystone[0] = 'A';
	command_complete_ble_eddystone[1] = 'T';
	command_complete_ble_eddystone[2] = '+';
	command_complete_ble_eddystone[3] = 'U';
	command_complete_ble_eddystone[4] = 'B';
	command_complete_ble_eddystone[5] = 'T';
	command_complete_ble_eddystone[6] = 'A';
	command_complete_ble_eddystone[7] = 'D';
	command_complete_ble_eddystone[8] = '=';
	//strncpy(command_complete_ble_eddystone, "AT+UBTAD=", 9);
	strncat(command_complete_ble_eddystone, payload_ble_eddystone, 44);
	command_complete_ble_eddystone[53] = 13;
	StartTransfers(command_complete_ble_eddystone);
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_at_command_set_beacon_payload() {
	uint8_t atReturn = 0;
	//modificar
	StartTransfers(
			"AT+UBTAD=1AFF4C000215EBEFD08370A247C89837E7B5634DF52400010001C5\r");
	HAL_Delay(50);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_command_scan_peer() {
	uint8_t atReturn = 0;
	int loc = 0;
	char ble_distance_buffer[2] = { 0 };

	StartTransfers("AT+UBTD=1,1\r");
	HAL_Delay(5000);
	if (flag_nina) {
		ubSizeToReceiver = 0;
		flag_nina = 0;
	}
	loc = find_location_word((char*) aStringToReceiver, "0303AAFE1116AAFE20");
	if (loc != 0) {
		ble_distance_buffer[0] = aStringToReceiver[loc + 16];
		ble_distance_buffer[1] = aStringToReceiver[loc + 17];
	}
	if ((ble_distance_buffer[0] > 47 && ble_distance_buffer[0] < 58)
			|| (ble_distance_buffer[0] > 64 && ble_distance_buffer[0] < 71)) {
		if ((ble_distance_buffer[1] > 47 && ble_distance_buffer[1] < 58)
				|| (ble_distance_buffer[1] > 64 && ble_distance_buffer[1] < 71)) {
			ble_distance[0] = ble_distance_buffer[0];
			ble_distance[1] = ble_distance_buffer[1];
			atReturn = 1;
			fn_fprint("\r\nble_distance: ");
			fn_fprint(ble_distance_buffer);
			fn_fprint("[hex]\r\n");
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}
//*****************     UART    *********************

void StartTransfers(char* info) {

	memset(aStringToSend, 0, 100);
	ubSizeToSend = strlen(info);
	for (int var = 0; var < ubSizeToSend; ++var) {
		aStringToSend[var] = info[var];
	}
	/* Start transfer only if not already ongoing */
	if (ubSend == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART4, aStringToSend[ubSend++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART4);
	}

}

void USART_CharTransmitComplete_Callback(void) {
	if (ubSend == ubSizeToSend) {
		ubSend = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART4);

	}
}

void USART_TXEmpty_Callback(void) {
	if (ubSend == (ubSizeToSend - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART4);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART4);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART4, aStringToSend[ubSend++]);
}

void USART_CharReception_Callback(void) {
	//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver[ubSizeToReceiver++] = LL_USART_ReceiveData8(
	USART4); //received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART4) == 13)) {
		flag_nina = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART4);

	}

	//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

void fn_at_uart_reception_nina(void) {

	if (flag_nina) {
		ubSizeToReceiver = 0;
		HAL_UART_Transmit(&hlpuart1, aStringToReceiver,
		BYTES_NINA_TO_RECEIVER, 1000);
		flag_nina = 0;
	}

}

//++++++++++++++++++++++ PERIMETRAL INFO PROCESS +++++++++++++++++++++

uint8_t fn_wait_for_connection() {
	uint8_t atReturn = 0;
	/*for (int i = 0; i < ubSizeToReceiver; ++i) {
	 if (aStringToReceiver[i - 10] == 43 && aStringToReceiver[i - 9] == 85
	 && aStringToReceiver[i - 8] == 85
	 && aStringToReceiver[i - 7] == 66
	 && aStringToReceiver[i - 6] == 84
	 && aStringToReceiver[i - 5] == 65
	 && aStringToReceiver[i - 4] == 67
	 && aStringToReceiver[i - 3] == 76
	 && aStringToReceiver[i - 2] == 67
	 && aStringToReceiver[i - 1] == 58) {*/
	if (strstr((char*) aStringToReceiver, "+UU")) {
		atReturn = 1;
		memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	}
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	return atReturn;
}

uint8_t fn_answer_at_data_mode_nina() {
	uint8_t answer = 0;
	if (flag_nina) {
		strcpy(at_data_receiver_ble_to_nina, (char*) aStringToReceiver);

		if (strstr((char*) aStringToReceiver, BLE_CFG_PEER) && answer == 0) {

			HAL_Delay(1000);
			find_between("AT$DP=", "\r", (char*) aStringToReceiver,
					peer_adress);
			if (peer_adress[0] == 0 && peer_adress[1] == 13) {
				flag_default_peer = RESET;
			} else {
				flag_default_peer = SET;
			}
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
		} else if (strstr((char*) aStringToReceiver, BLE_ALL) && answer == 0) {

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, st_sensor_char_values.battery);
			strcat(at_data_receiver_ble_to_nina, " , ");

			strcat(at_data_receiver_ble_to_nina,
					st_sensor_char_values.temperature);
			strcat(at_data_receiver_ble_to_nina, " , ");

			strcat(at_data_receiver_ble_to_nina,
					st_sensor_char_values.distance);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_VOLT) && answer == 0) {

			HAL_Delay(1000);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, st_sensor_char_values.battery);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_TEMP) && answer == 0) {

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina,
					st_sensor_char_values.temperature);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_DIST) && answer == 0) {

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina,
					st_sensor_char_values.distance);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_CFG_P)
				&& answer == 0) {

			HAL_Delay(1000);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_CFG_C)
				&& answer == 0) {

			HAL_Delay(1000);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(CENTRAL_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_CFG_I_BCN)
				&& answer == 0) {

			HAL_Delay(1000);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(IBEACON_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_CFG_E_BCN)
				&& answer == 0) {

			HAL_Delay(1000);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(EDDYSTONE_BEACON_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_INFO) && answer == 0) {
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_CFG) && answer == 0) {
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (strstr((char*) aStringToReceiver, BLE_RST) && answer == 0) {
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, "RESETING");
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(RESET_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_RST_FACTORY)
				&& answer == 0) {

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, "FACTORY RESET");
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);
			fn_Change_Nina_Config_Type(FACTORY_MODE);
			answer = 1;

		} else if (strstr((char*) aStringToReceiver, BLE_AT) && answer == 0) {
			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_OK);

		} else if (answer == 0) {

			strcat(at_data_receiver_ble_to_nina, BLE_CRLF);
			strcat(at_data_receiver_ble_to_nina, BLE_ERROR);

		}
		HAL_Delay(100);
		StartTransfers(at_data_receiver_ble_to_nina);
		ubSizeToReceiver = 0;
		flag_nina = 0;
		memset(at_data_receiver_ble_to_nina, 0, DATA_SIZE_2);
		memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	}
	return answer;
}

//++++++++++++++++++++++ CENTRAL INFO PROCESS +++++++++++++++++++++

uint8_t fn_discovery_devices_nina() {
	uint8_t atReturn = 0;

	StartTransfers("AT+UBTD=4,1,2000\r");

	HAL_Delay(3000);
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	//uint8_t counter = countOccurrences((char*) aStringToReceiver, "+UBTD:");
	if (!flag_default_peer) {
		memset(peer_adress, 0, 18);
		if (strstr((char*) aStringToReceiver, "	wBEacon")) {
			HAL_Delay(200);
			find_between("+UBTD:", "wBEacon", (char*) aStringToReceiver,
					peer_adress);
			if (peer_adress[12] == 112) {
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\npeer_adress: ", 15,
						100);
				HAL_Delay(200);
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) peer_adress, 13, 500);
				HAL_Delay(200);
				HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 50);
				atReturn = 1;
			}
		}
	} else if (flag_default_peer) {
		if (strstr((char*) aStringToReceiver, peer_adress)) {
			HAL_Delay(200);
			peer_adress[12] = 112;

			HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\npeer_adress: ", 15,
					100);
			HAL_Delay(200);
			HAL_UART_Transmit(&hlpuart1, (uint8_t*) peer_adress, 13, 500);
			HAL_Delay(200);
			HAL_UART_Transmit(&hlpuart1, (uint8_t*) "\r\n", 2, 50);
			atReturn = 1;

		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);

	return atReturn;
}

uint8_t fn_at_connect_peer() {

	char command[27] = "AT+UDCP=sps://xxxxxxxxxxxx";
	for (int var = 0; var < 13; ++var) {
		command[var + 14] = peer_adress[var];
	}
	command[26] = 13;
	uint8_t atReturn = 0;
	StartTransfers(command);
	HAL_Delay(10000);
	fn_at_uart_reception_nina();
	HAL_Delay(100);
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_at_disconect_peer() {
	uint8_t atReturn = 0;
	StartTransfers("AT+UDCPC=1\r");
	HAL_Delay(100);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

//********************** DATA AT COMMANDS ************************

uint8_t fn_data_at_command() {
	uint8_t atReturn = 0;
	StartTransfers("AT\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_temperature() {
	uint8_t atReturn = 0;
	StartTransfers("AT$T?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_battery() {
	uint8_t atReturn = 0;
	StartTransfers("AT$V?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_get_at_distance() {
	uint8_t atReturn = 0;
	StartTransfers("AT$D?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;
}

uint8_t fn_data_get_at_all_sensors() {
	uint8_t atReturn = 0;
	StartTransfers("AT$A?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_get_at_config() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF?\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {

		}
		if (aStringToReceiver[i] == 82 && aStringToReceiver[i + 1] == 84
				&& aStringToReceiver[i + 2] == 82) {
			fn_Change_Nina_Config_Type(PERIMETRAL_MODE);
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_central_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=1\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_perimetral_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=2\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_ibeacon_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=3\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

uint8_t fn_data_set_at_eddystone_mode_operation() {
	uint8_t atReturn = 0;
	StartTransfers("AT$CF=4\r");
	HAL_Delay(1000);
	fn_at_uart_reception_nina();
	for (int i = 0; i < BYTES_NINA_TO_RECEIVER; ++i) {
		if (aStringToReceiver[i - 1] == 79 && aStringToReceiver[i] == 75) {
			atReturn = 1;
		}
	}
	memset(aStringToReceiver, 0, BYTES_NINA_TO_RECEIVER);
	return atReturn;

}

