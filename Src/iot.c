/*
 * sigfox.c
 *
 *  Created on: 9 de mar de 2020
 *      Author: oscar
 */
#include <iot.h>
#include "stm32l0xx_it.h"

__IO uint8_t ubSend_SIGFOX = 0;
uint8_t aStringToSend_SIGFOX[100];
uint8_t ubSizeToSend_SIGFOX = 0; //sizeof(aStringToSend_SARA);
uint8_t aStringToReceiver_SIGFOX[SIGFOX_BYTES_TO_RECEIVER];
uint8_t ubSizeToReceiver_SIGFOX = 0;
uint8_t flag_sigfox;

//********************* WISOL AT COMMAND ********************************

uint8_t fn_at_command_sigfox(char * at_command) {
	uint8_t atReturn = 0;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	int size_at_command = strlen(at_command);
	size_at_command += 2;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r\n");

	Start_SIGFOX_Transfers(new_command);
	HAL_Delay(100);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var - 1] == 79
				&& aStringToReceiver_SIGFOX[var] == 75) {
			atReturn = 1;
		}
	}

	return atReturn;
}

//********************* GET WISOL INFO ********************************
uint8_t fn_check_sigfox_status() {
	uint8_t check = 0;

	SIGFOX_ON
	HAL_Delay(200);
	check = fn_at_sigfox();
	HAL_Delay(200);
	SIGFOX_OFF

	return check;

}

void fn_info_sigfox() {
	SIGFOX_ON
	uint8_t check = 0, time = 2;
	HAL_Delay(200);
	check = fn_at_sigfox();
	while (!check && time != 0) {
		HAL_Delay(100);
		time--;
		check = fn_at_sigfox();
	}
	fn_fprint("SIGFOX ID: ");
	if (!fn_get_id_sigfox()) {
		fn_at_sigfox();
		fn_get_id_sigfox();
	}
	HAL_Delay(100);
	fn_fprint("SIGFOX PAC: ");
	if (!fn_get_pac_sigfox()) {
		fn_at_sigfox();
		fn_get_pac_sigfox();
	}
	HAL_Delay(100);
	fn_fprint("SIGFOX TEMPERATURE: ");
	fn_get_temperature_sigfox();
	HAL_Delay(100);
	fn_fprint("SIGFOX VOLTS: ");
	fn_get_volt_sigfox();
	HAL_Delay(100);
	SIGFOX_OFF
}

uint8_t fn_get_id_sigfox() {
	uint8_t responce = 0;
	//char command[9] = "AT$I=10\r\n";

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$I=10\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.id[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_get_pac_sigfox() {
	uint8_t responce = 0;
	//char command[9] = "AT$I=11\r\n";

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$I=11\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;
	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.pac[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	return responce;
}

uint8_t fn_at_sigfox() {
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	//char command[4] = "AT\r";
	fn_fprint("\r\nAT ");
	Start_SIGFOX_Transfers("AT\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();
	st_flag.SigFox = 0;
	st_flag.LoRa = 0;
	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 61) {
			responce = 1;
			st_flag.LoRa = 1;
			st_flag.SigFox = 0;
			break;
		}
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			st_sigfox_parameters.at[0] = aStringToReceiver_SIGFOX[var];
			st_sigfox_parameters.at[1] = aStringToReceiver_SIGFOX[var + 1];
			st_flag.LoRa = 0;
			st_flag.SigFox = 1;
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_get_volt_sigfox() {
	uint8_t responce = 0;
	//char command[7] = "AT$V?\r\n";
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$V?\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.volts[r_p] = aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	int_volt_sigfox = atoi(st_sigfox_parameters.volts);
	int_volt_sigfox /= 10;
	return responce;
}

uint8_t fn_get_temperature_sigfox() {
	uint8_t responce = 0;
	//char command[8] = "AT$T?\r\n";
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);
	Start_SIGFOX_Transfers("AT$T?\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	uint8_t r_p = 0;

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if ((aStringToReceiver_SIGFOX[var] >= 48
				&& aStringToReceiver_SIGFOX[var] <= 57)
				|| (aStringToReceiver_SIGFOX[var] >= 65
						&& aStringToReceiver_SIGFOX[var] <= 70)) {
			st_sigfox_parameters.temperature[r_p] =
					aStringToReceiver_SIGFOX[var];
			r_p++;
			responce = 1;
		}
	}

	int_temp_sigfox = atoi(st_sigfox_parameters.temperature);
	int_temp_sigfox /= 10;
	if (st_sigfox_parameters.temperature)
		responce = 1;
	return responce;

}

void fn_status_sigfox() {
	if (HAL_GPIO_ReadPin(WISOL_LED_CPU_GPIO_Port, WISOL_LED_CPU_Pin)) {
		LED_ON
	} else
		LED_OFF
}

void fn_reset_sigfox() {
	SIGFOX_RESET_ON
	HAL_Delay(50);
	SIGFOX_RESET_OFF
	//HAL_GPIO_DeInit(EN_SFOX_GPIO_Port, EN_SFOX_Pin);
	//fn_fprint("\r\nsigfox reset\r\n");
}

//******************** SEND SIGFOX PAYLOADS **************************

void fn_send_report_frame_sigfox() {
	fn_fprint("\r\nSIGFOX REPORT FRAME\r\n");
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}
	fn_send_config_sigfox();
	memset(st_frame_type.downlink_data, 0, 16);
	char sigfox_complete_report_frame[30] = { 0 };
	char sigfox_frame_report_values[22] = { 0 };

	fn_encoder_report_frame(sigfox_frame_report_values);

	sigfox_complete_report_frame[0] = 65;
	sigfox_complete_report_frame[1] = 84;
	sigfox_complete_report_frame[2] = 36;
	sigfox_complete_report_frame[3] = 83;
	sigfox_complete_report_frame[4] = 70;
	sigfox_complete_report_frame[5] = 61;
	for (int var = 0; var < 22; ++var) {
		sigfox_complete_report_frame[var + 6] = sigfox_frame_report_values[var];
	}
	sigfox_complete_report_frame[28] = 13;
	sigfox_complete_report_frame[29] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(sigfox_complete_report_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();
	fn_fprint("frame report:  ");
	fn_fprint(sigfox_frame_report_values);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	SIGFOX_OFF
}

void fn_send_start_frame_sigfox() {
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}
	/*	fn_get_id_sigfox();
	 fn_get_pac_sigfox();
	 fn_get_temperature_sigfox();
	 fn_get_volt_sigfox();*/
	fn_send_config_sigfox();
	char start_machine_frame[30] = { 0 };
	char buffer_start[7] = { 0 };
	int temp_buff = (int_temp_sigfox);
	int volt_buff = (int_volt_sigfox - 120);

	start_machine_frame[0] = 65;
	start_machine_frame[1] = 84;
	start_machine_frame[2] = 36;
	start_machine_frame[3] = 83;
	start_machine_frame[4] = 70;
	start_machine_frame[5] = 61;

	start_machine_frame[6] = 48;
	start_machine_frame[7] = 50;

	decHex(volt_buff, buffer_start);
	check_size_info(2, buffer_start);
	strcat(start_machine_frame, buffer_start);

	decHex(temp_buff, buffer_start);
	check_size_info(2, buffer_start);
	strcat(start_machine_frame, buffer_start);

	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");
	strcat(start_machine_frame, "00");

	start_machine_frame[28] = 13;
	start_machine_frame[29] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(start_machine_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();

	fn_fprint("frame start:  ");
	fn_fprint(start_machine_frame);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	SIGFOX_OFF
}

void fn_send_daily_frame_sigfox() {
	fn_fprint("\r\nSIGFOX DAILY FRAME\r\n");
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}

	memset(st_frame_type.downlink_data, 0, 16);

	char sigfox_frame_daily_values[22] = { 0 };

	char sigfox_complete_daily_frame[32] = { 0 };

	fn_encoder_report_frame(sigfox_frame_daily_values);

	HAL_Delay(500);
	fn_reset_sigfox();
	fn_at_sigfox();
	fn_send_config_sigfox();
	char downlinkRX[19] = { 0 };
	char config[64] = { 0 };
	char buff_downlink[32] = { 0 };
	uint8_t buff_flag = 0;

	/*char buff_dec[64] = { 0 };
	 char undTimeByte[2] = { 0 };
	 char volume_ref[3] = { 0 };
	 char time_stamp[7] = { 0 };
	 char timebyte[6] = { 0 };*/

	sigfox_complete_daily_frame[0] = 65;
	sigfox_complete_daily_frame[1] = 84;
	sigfox_complete_daily_frame[2] = 36;
	sigfox_complete_daily_frame[3] = 83;
	sigfox_complete_daily_frame[4] = 70;
	sigfox_complete_daily_frame[5] = 61;

	/*	sigfox_complete_report_frame[6] = 48;
	 sigfox_complete_report_frame[7] = 50;*/

	for (int var = 0; var < 22; ++var) {
		sigfox_complete_daily_frame[var + 6] = sigfox_frame_daily_values[var];

	}
	//strcat(sigfox_complete_report_frame, sigfox_frame_report_values);

	sigfox_complete_daily_frame[28] = 44;
	sigfox_complete_daily_frame[29] = 49;
	sigfox_complete_daily_frame[30] = 13;
	sigfox_complete_daily_frame[31] = 10;

	if (buff_flag == 0) {
		Start_SIGFOX_Transfers(sigfox_complete_daily_frame);
		HAL_Delay(35000);
		fn_at_uart_reception_sigfox();
		for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SIGFOX[var] != 0) {
				strcpy(config, (char*) aStringToReceiver_SIGFOX);
				buff_flag = 1;
				break;
			}
		}
	}

	if (buff_flag == 0) {
		Start_SIGFOX_Transfers(sigfox_complete_daily_frame);
		HAL_Delay(35000);
		fn_at_uart_reception_sigfox();
		for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SIGFOX[var] != 0) {
				strcpy(config, (char*) aStringToReceiver_SIGFOX);
				buff_flag = 1;
				break;
			}
		}
	}

	if (buff_flag == 0) {
		Start_SIGFOX_Transfers(sigfox_complete_daily_frame);
		HAL_Delay(35000);
		fn_at_uart_reception_sigfox();
		for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SIGFOX[var] != 0) {
				strcpy(config, (char*) aStringToReceiver_SIGFOX);
				buff_flag = 1;
				break;
			}
		}
	}

	if (buff_flag == 1) {
		find_between("R", "\r", config, buff_downlink);
		for (int var = 0; var < 32; ++var) {
			if (config[var] == 82 && config[var + 1] == 88
					&& config[var + 2] == 61) {

				downlinkRX[0] = config[var];
				downlinkRX[1] = config[var + 1];
				downlinkRX[2] = config[var + 2];

				downlinkRX[3] = config[var + 3];
				downlinkRX[4] = config[var + 4];

				downlinkRX[5] = config[var + 6];
				downlinkRX[6] = config[var + 7];

				downlinkRX[7] = config[var + 9];
				downlinkRX[8] = config[var + 10];

				downlinkRX[9] = config[var + 12];
				downlinkRX[10] = config[var + 13];

				downlinkRX[11] = config[var + 15];
				downlinkRX[12] = config[var + 16];

				downlinkRX[13] = config[var + 18];
				downlinkRX[14] = config[var + 19];

				downlinkRX[15] = config[var + 21];
				downlinkRX[16] = config[var + 22];

				downlinkRX[17] = config[var + 21];
				downlinkRX[18] = config[var + 21];

			}
		}

		get_downlink_payload(downlinkRX);

		/*
		 RemoveSpaces(buff_downlink);
		 strcpy(downlinkRX, buff_downlink);

		 if (strlen(downlinkRX) != 0) {
		 fn_fprint("\r\nDOWNLINK VALUES: ");
		 fn_fprint(downlinkRX);

		 time_stamp[0] = downlinkRX[6];
		 time_stamp[1] = downlinkRX[7];
		 time_stamp[2] = downlinkRX[4];
		 time_stamp[3] = downlinkRX[5];
		 time_stamp[4] = downlinkRX[2];
		 time_stamp[5] = downlinkRX[3];

		 volume_ref[0] = downlinkRX[10];
		 volume_ref[1] = downlinkRX[11];

		 vol_thresh[0] = downlinkRX[15];
		 ang_thresh[0] = downlinkRX[14];
		 tmp_thresh[0] = downlinkRX[13];
		 gas_thresh[0] = downlinkRX[12];
		 HAL_Delay(10);
		 hexBin(downlinkRX, RX_buff_bin);

		 fn_fprint("\r\nDOWNLINK BINARY: ");
		 fn_fprint(RX_buff_bin);

		 //choice of transmission resource
		 if (RX_buff_bin[4] == 49)
		 st_flag.SigFox = 1;
		 if (RX_buff_bin[4] == 48)
		 st_flag.SigFox = 0;
		 if (RX_buff_bin[3] == 49)
		 st_flag.GPRS = 1;
		 if (RX_buff_bin[3] == 48)
		 st_flag.GPRS = 0;
		 if (RX_buff_bin[2] == 49)
		 st_flag.BLE = 1;
		 if (RX_buff_bin[2] == 48)
		 st_flag.BLE = 0;

		 st_flag.SigFox = RX_buff_bin[4];
		 st_flag.GPS = RX_buff_bin[3];
		 st_flag.BLE = RX_buff_bin[2];

		 undTimeByte[0] = RX_buff_bin[32]; //0
		 undTimeByte[1] = RX_buff_bin[33]; //1
		 timebyte[0] = RX_buff_bin[34]; //0
		 timebyte[1] = RX_buff_bin[35]; //1
		 timebyte[2] = RX_buff_bin[36]; //1
		 timebyte[3] = RX_buff_bin[37]; //1
		 timebyte[4] = RX_buff_bin[38]; //1
		 timebyte[5] = RX_buff_bin[39]; //0
		 HAL_Delay(10);
		 timebyte_int = atoi(timebyte);
		 HAL_Delay(10);
		 undTimeByte_int = atoi(undTimeByte);
		 HAL_Delay(10);
		 time_byte_value = binaryToDec(timebyte_int); //transmission period in hours
		 HAL_Delay(10);
		 int Vol_Reference = hexDec(volume_ref);
		 int Vol_Threshold = hexDec(vol_thresh);
		 int Gas_Threshold = hexDec(gas_thresh);
		 int Ang_Threshold = hexDec(ang_thresh);
		 int Tmp_Threshold = hexDec(tmp_thresh);

		 TimeStamp = hexDec(time_stamp); //timestamp minutes-sigmais protocol

		 dailyTime = fn_get_seconsForTimeStemp(TimeStamp); //seconds elapsed in the day
		 HAL_Delay(10);

		 if (time_byte_value != 0) {
		 if (undTimeByte_int == 00) //if 00 = time unid its seconds
		 {
		 keepAliveTimer = time_byte_value;
		 }
		 if (undTimeByte_int == 01) // if 01 = time unid its minutes
		 {
		 keepAliveTimer = (time_byte_value * 60);
		 }
		 if (undTimeByte_int == 10) // if 10 = time unid its hours
		 {
		 keepAliveTimer = (time_byte_value * 3600);
		 }
		 if (undTimeByte_int == 11) // if 11 = time unid its days
		 {
		 keepAliveTimer = (time_byte_value * 86400);
		 }
		 }
		 }*/
		/*		fn_fprint("\r\nSECONDS TO SEND: ");
		 fn_fprintnumber((int) st_timers.keep_alive_seconds);
		 HAL_Delay(10);
		 fn_fprint("\r\nSECONDS TODAY: ");
		 fn_fprintnumber((int) st_timers.seconds_today);
		 HAL_Delay(10);*/
	}

	SIGFOX_OFF
	fn_fprint("\r\nSIGFOX OFF");

}

//********************* SEND WISOL CONFIGURATIONS ********************************

uint8_t fn_send_config_sigfox() {
	uint8_t responce = 1;

	/*char ok[4];
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$P=0\r\n", 8, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$DR=905200000\r\n", 17, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$IF=902200000\r\n", 17, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$WR\r\n", 7, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);
	 HAL_UART_Transmit(&huart5, (uint8_t*) "AT$RC\r\n", 7, 100);
	 HAL_UART_Receive(&huart5, (uint8_t*) ok, 4, 100);
	 HAL_Delay(100);

	 #ifdef DONGLE_KEY
	 //public key or private key
	 HAL_UART_Transmit(&huart5,(uint8_t*)"ATS410=1\r\n",11,100);//ATS410=1 private key ; ATS410=0 public key
	 //HAL_UART_Receive(&huart5,(uint8_t*)ok,4,10);
	 HAL_Delay(500);
	 #endif
	 */
	/*
	 #ifdef DONGLE_KEY
	 //public key or private key
	 HAL_UART_Transmit(&huart5, (uint8_t*) "ATS410=1\r\n", 11, 100);//ATS410=1 private key ; ATS410=0 public key
	 //HAL_UART_Receive_IT(&huart5,(uint8_t*)ok,4,10);
	 HAL_Delay(500);
	 #endif
	 */
	fn_set_reset_module_sigfox();
	fn_set_RX_frequency_sigfox();
	fn_set_TX_frequency_sigfox();
	fn_set_save_configuration_sigfox();
	fn_set_reset_FCC_sigfox();
	fn_set_dongle_module_sigfox();
	return responce;
}

uint8_t fn_set_dongle_module_sigfox() {
	fn_fprint("dongle - ");
	uint8_t responce = 0;
#ifdef DONGLE_KEY
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("ATS410=1\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
#endif
	return responce;
}

uint8_t fn_set_reset_module_sigfox() {
	fn_fprint("reset module sigfox - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$P=0\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_RX_frequency_sigfox() {
	fn_fprint("set RX frequence - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$DR=905200000\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_TX_frequency_sigfox() {
	fn_fprint("set TX frequence - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$IF=902200000\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_save_configuration_sigfox() {
	fn_fprint("save config - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$WR\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

uint8_t fn_set_reset_FCC_sigfox() {
	fn_fprint("reset FCC - ");
	uint8_t responce = 0;
	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers("AT$RC\r\n");
	HAL_Delay(300);
	fn_at_uart_reception_sigfox();

	for (int var = 0; var < SIGFOX_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SIGFOX[var] == 79
				&& aStringToReceiver_SIGFOX[var + 1] == 75) {
			responce = 1;
		}
	}
	return responce;
}

//*****************     UART    *********************

void Start_SIGFOX_Transfers(char* sfox_info) {

	memset(aStringToSend_SIGFOX, 0, 100);
	ubSizeToSend_SIGFOX = strlen(sfox_info);
	for (int var = 0; var < ubSizeToSend_SIGFOX; ++var) {
		aStringToSend_SIGFOX[var] = sfox_info[var];
	}
	/* Start transfer only if not already ongoing */
	if (ubSend_SIGFOX == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART1, aStringToSend_SIGFOX[ubSend_SIGFOX++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART1);
	}

}

void USART1_CharTransmitComplete_Callback(void) {
	if (ubSend_SIGFOX == ubSizeToSend_SIGFOX) {
		ubSend_SIGFOX = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART1);

	}
}

void USART1_TXEmpty_Callback(void) {
	if (ubSend_SIGFOX == (ubSizeToSend_SIGFOX - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART1);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART1);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART1, aStringToSend_SIGFOX[ubSend_SIGFOX++]);
}

void USART1_CharReception_Callback(void) {
	//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver_SIGFOX[ubSizeToReceiver_SIGFOX++] = LL_USART_ReceiveData8(
	USART1);	//received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART1) == 13)) {
		flag_sigfox = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART1);

	}

	//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

void fn_at_uart_reception_sigfox(void) {

	if (flag_sigfox) {
		ubSizeToReceiver_SIGFOX = 0;
		HAL_UART_Transmit(&hlpuart1, aStringToReceiver_SIGFOX,
		SIGFOX_BYTES_TO_RECEIVER, 1000);
		flag_sigfox = 0;
	}

}

//********************************************************************************
//******************** SEND LORA PAYLOADS **************************

void fn_send_report_frame_lora() {
	fn_fprint("\r\n LORA REPORT FRAME\r\n");
	uint8_t check = 0, time = 10;
	SIGFOX_ON
	HAL_Delay(200);

	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}

	memset(st_frame_type.downlink_data, 0, 16);
	char sigfox_complete_report_frame[30] = { 0 };
	char sigfox_frame_report_values[22] = { 0 };

	fn_encoder_report_frame(sigfox_frame_report_values);

	sigfox_complete_report_frame[0] = 65;
	sigfox_complete_report_frame[1] = 84;
	sigfox_complete_report_frame[2] = 36;
	sigfox_complete_report_frame[3] = 83;
	sigfox_complete_report_frame[4] = 70;
	sigfox_complete_report_frame[5] = 61;
	for (int var = 0; var < 22; ++var) {
		sigfox_complete_report_frame[var + 6] = sigfox_frame_report_values[var];
	}
	sigfox_complete_report_frame[28] = 13;
	sigfox_complete_report_frame[29] = 10;

	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(sigfox_complete_report_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();
	fn_fprint("frame report:  ");
	fn_fprint(sigfox_frame_report_values);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	SIGFOX_OFF
}

void fn_send_report_frame_tago() {
	SIGFOX_ON
	LED_ON
	HAL_Delay(1000);
	fn_fprint("\r\n TAGO REPORT FRAME\r\n");

	int val = 0;
	unsigned int ble_dist_int = 0;
	int len=2;
	len--;

	char tago_complete_report_frame[32] = { 0 };
	char tago_frame_report_values[24] = { 0 };

	char buffer_longitude[6] = { 0 };
	char buffer_latitude[6] = { 0 };
	char buffer_distance[4] = { 0 };
	char buffer_temperature[2] = { 0 };
	char buffer_battery[2] = { 0 };
	char buffer_ble_dist[4] = { 0 };

	for (int i = 0; i < 2; i++) {
		if (ble_distance[i] >= '0' && ble_distance[i] <= '9') {
			val = ble_distance[i] - 48;
		} else if (ble_distance[i] >= 'a' && ble_distance[i] <= 'f') {
			val = ble_distance[i] - 97 + 10;
		} else if (ble_distance[i] >= 'A' && ble_distance[i] <= 'F') {
			val = ble_distance[i] - 65 + 10;
		}
		ble_dist_int += val * pow(16, len);
		len--;

	}

	//decHex(st_data_sensor_e.battery, buffer_battery);
	itoa((st_stm_adc_variables.battery - 3000) / 10, buffer_battery, 10);
	check_size_info(2, buffer_battery);
	strcat(tago_frame_report_values, buffer_battery);

	//decHex(st_data_sensor_e.temperature, buffer_temperature);
	itoa(st_stm_adc_variables.temperature, buffer_temperature, 10);
	check_size_info(2, buffer_temperature);
	strcat(tago_frame_report_values, buffer_temperature);

	//decHex(st_data_sensor_e.latitude, buffer_latitude);
	itoa(st_data_sensor_e.latitude, buffer_latitude, 10);
	check_size_info(6, buffer_latitude);
	strcat(tago_frame_report_values, buffer_latitude);

	//decHex(st_data_sensor_e.longitude, buffer_longitude);
	itoa(st_data_sensor_e.longitude, buffer_longitude, 10);
	check_size_info(6, buffer_longitude);
	strcat(tago_frame_report_values, buffer_longitude);

	//decHex(st_data_sensor_e.distance, buffer_distance);
	itoa(st_data_sensor_e.distance, buffer_distance, 10);
	check_size_info(4, buffer_distance);
	strcat(tago_frame_report_values, buffer_distance);

	/*	decHex(st_data_sensor_e.angle, buffer_angle);
	 check_size_info(2, buffer_angle);
	 strcat(frame, buffer_angle);*/



	itoa(ble_dist_int, buffer_ble_dist, 10);
	check_size_info(4, buffer_ble_dist);
	strcat(tago_frame_report_values, buffer_ble_dist);

	uint8_t check = 0, time = 10;


	HAL_Delay(100);
	while (!check && time != 0) {
		HAL_Delay(1000);
		time--;
		check = fn_at_sigfox();
	}

	memset(st_frame_type.downlink_data, 0, 16);

	tago_complete_report_frame[0] = 65;
	tago_complete_report_frame[1] = 84;
	tago_complete_report_frame[2] = 36;
	tago_complete_report_frame[3] = 83;
	tago_complete_report_frame[4] = 70;
	tago_complete_report_frame[5] = 61;
	tago_complete_report_frame[30] = 13;
	tago_complete_report_frame[31] = 10;
	for (int var = 0; var < 24; ++var) {
		tago_complete_report_frame[var + 6] = tago_frame_report_values[var];
	}


	memset(aStringToReceiver_SIGFOX, 0, SIGFOX_BYTES_TO_RECEIVER);

	Start_SIGFOX_Transfers(tago_complete_report_frame);
	HAL_Delay(500);
	fn_at_uart_reception_sigfox();
	fn_fprint("frame report:  ");
	fn_fprint(tago_frame_report_values);
	fn_fprint("\r\n");
	HAL_Delay(1000);
	//SIGFOX_OFF
	LED_OFF
}
