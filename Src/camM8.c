/*
 * camM8.c
 *
 *  Created on: 6 de mai de 2020
 *      Author: oscar
 */

#include "camM8.h"



unsigned int fn_get_latutude_camM8() {
	if (latitude == 0)
		fn_get_gps_camM8();
	return latitude;
}

unsigned int fn_get_longitude_camM8() {
	fn_get_gps_camM8();
	return longitude;
}

unsigned int fn_get_altitude_camM8() {
	fn_get_gps_camM8();
	return altitude;
}

unsigned int fn_get_hour_camM8() {
	fn_get_gps_camM8();
	return hour;
}

unsigned int fn_get_minute_camM8() {
	fn_get_gps_camM8();
	return minute;
}

unsigned int fn_get_second_camM8() {
	fn_get_gps_camM8();
	return second;
}

unsigned int fn_get_numSV_camM8() {
	fn_get_gps_camM8();
	return numSV;
}

unsigned int fn_get_quality_camM8() {
	fn_get_gps_camM8();
	return quality;
}

unsigned int fn_get_speed_camM8() {
	fn_get_gps_camM8();
	return speed;
}

void fn_get_gps_camM8() {
	char buffer_gps[1024] = "";
	char aux_buff_gps[74] = "";
	unsigned int hora;
	float buffer_latitude, buffer_longitude;
	unsigned int buffer_qualidade, buffer_numSV, buffer_altitude;

	struct {
		char lat[11], lon[11], hra[10], NS[1], EW[1], numSV[2], speed[6],
				quality[2], alt[8];
	} aux;
	HAL_UART_Init(&hlpuart1);
	HAL_UART_Receive(&hlpuart1, (uint8_t*) buffer_gps, 1024, 1500);
	HAL_UART_Transmit(&hlpuart1, (uint8_t*) buffer_gps, 1024, 1500);
	/*	 Message Structure:
	 *
	 $xxVTG,cogt,cogtUnit,cogm,cogmUnit,sogn,sognUnit,sogk,sogkUnit,posMode*cs<CR><LF>
	 Example:
	 $GPVTG,77.52,T,,M,0.004,N,0.008,K,A*06
	 ****************************************************************************************
	 *FieldNo. 	* Name 		*	Unit	*	Format		*	Example	*		Description		*
	 *   4  	* cogmUnit  *  -    	* 	character  	*  	  M   	*   Course over ground 	*
	 *   5  	* sogn      * 	knots 	* 	numeric    	* 	0.004 	*   Speed over ground	*
	 *   7  	* sogk      * 	km/h  	* 	numeric    	* 	0.008 	*   Speed over ground	*
	 ****************************************************************************************
	 $xxGGA,time,lat,NS,lon,EW,quality,numSV,HDOP,alt,altUnit,sep,sepUnit,diffAge,diffStation*cs<CR><LF>
	 Example:
	 $GPGGA,092725.00,4717.11399,N,00833.91590,E,1,08,1.01,499.6,M,48.0,M,,*5B*/

	find_between("GGA,", "\n", buffer_gps, aux_buff_gps);

	if (strlen(aux_buff_gps) > 30) {
		strncpy(aux.hra, aux_buff_gps, 9);

		hora = atoi(aux.hra);              // hora (hh.mm.ss) 165920.00
		hour = hora / 10000;
		minute = (hora / 100) - (hour * 100);
		second =  hora -((hour * 10000)+(minute*100));

		if (hora != 0) {
			switch (hour) {
			case 2:
				hour = 23;
				break;
			case 1:
				hour = 22;
				break;
			case 0:
				hour = 21;
				break;
			default:
				hour -= 3;
				break;
			}
		}
		TimeCounter = (hour * 360 + minute * 60 + second);
	}

	if (strlen(aux_buff_gps) > 40) {
		uint8_t c = 0;
		char* token;
		token = strtok(aux_buff_gps, ",");

		while (token != NULL) {
			if (c == 0) {
				//		strcpy(aux.hra, token);
			}
			if (c == 1) {
				strcpy(aux.lat, token);
			}
			if (c == 2) {
				strcpy(aux.NS, token);
			}
			if (c == 3) {
				strcpy(aux.lon, token);
			}
			if (c == 4)
				strcpy(aux.EW, token);
			if (c == 5) {
				strcpy(aux.quality, token);
			}
			if (c == 6) {
				strcpy(aux.numSV, token);
			}
			if (c == 7) {
			}
			if (c == 8) {
				strcpy(aux.alt, token);
			}
			if (c == 9)
				token = NULL;
			c++;
			token = strtok(NULL, ",");
		}

		buffer_latitude = atof(aux.lat);     	// latitude
		buffer_longitude = atof(aux.lon);     	// longitude
		buffer_altitude = atof(aux.alt);		// altitude
		buffer_numSV = atoi(aux.numSV);			// sateletes number
		buffer_qualidade = atoi(aux.quality);	// sinal quality

		buffer_latitude /= 100;
		buffer_longitude /= 100;

		unsigned int lat_graus = buffer_latitude;
		unsigned int lon_graus = buffer_longitude;

		float lat_min = (buffer_latitude - lat_graus) / 60;
		float lon_min = (buffer_longitude - lon_graus) / 60;

		lat_min *= 100;
		lon_min *= 100;

		float new_buffer_latitude = lat_graus + lat_min;
		float new_buffer_longitude = lon_graus + lon_min;

		latitude = new_buffer_latitude * 100000;
		longitude = new_buffer_longitude * 100000;
		altitude = buffer_altitude;
		quality = buffer_qualidade;
		numSV = buffer_numSV;
	}
}

uint8_t fn_get_gps_values_camM8() {
	uint8_t responce = 0;
	longitude = 0;
	latitude = 0;
	fn_get_gps_camM8();
	if (latitude != 0 && longitude != 0) {
		st_data_sensor_e.latitude = latitude;
		st_data_sensor_e.longitude = longitude;
		responce = 1;
	}
	return responce;
}

uint32_t secondsToDayTimeGPS() {
	uint32_t responce;
	responce = (hour * 360 + minute * 60 + second);
	return responce;
}

uint8_t fn_check_camM8_status() {
	uint8_t check = 0;
	char buffer_gps[1024] = "";
	GPS_ON
	HAL_UART_Receive(&hlpuart1, (uint8_t*) buffer_gps, 1024, 1500);
	if (!strstr(buffer_gps, "u-blox")) {
		check = 1;
	}
	GPS_OFF
	HAL_Delay(200);
	return check;
}
//************************** AUXILIAR CAM_M8 GPS ***************************************

void fn_check_gps_program() {
/*	unsigned int buff_timer_enable=0;
	//unsigned int buff_sum_gps_enable = (300+TimeCounter);
	GPS_flag_get_values_ok = 1;
	fn_fprint("GPS DATA");
	GPS_ON
	while (fn_get_gps_values_camM8() == 0 && GPS_flag_get_values_ok == 1) {
		if (buff_timer_enable>=3000) {
			GPS_flag_get_values_ok = 0;
		}else 	if(buff_timer_enable<300){
			buff_timer_enable++;
			HAL_Delay(5000);
		}
	}
	GPS_OFF*/
}

