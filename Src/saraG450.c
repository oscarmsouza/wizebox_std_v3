/*
 * saraG450.c
 *
 *  Created on: 30 de mar de 2020
 *      Author: oscar
 */

#include "saraG450.h"
#include "stm32l0xx_it.h"

__IO uint8_t ubSend_SARA = 0;
uint8_t aStringToSend_SARA[100];
uint8_t ubSizeToSend_SARA = 0; //sizeof(aStringToSend_SARA);
uint8_t aStringToReceiver_SARA[SARA_BYTES_TO_RECEIVER];
uint8_t ubSizeToReceiver_SARA = 0;

void fn_power_on_sara(void) { //to power on call this
	fn_fprint("\r\nPOWER ON SARA MODULE\r\n");
	GPRS_ON
	HAL_Delay(1000);
	GPRS_RESET_OFF
	HAL_Delay(5000);
	GPRS_PWR_ON
	HAL_Delay(20000);
	fn_fprint("SARA MUDULE RUN\r\n");
}

/*
 * first at commands you send and verify cell chip
 * if one this events does not work ok the module torn off
 */
void fn_start_at_commands_sara(void) {
	//fn_power_on_sara();
	int timeToScape = 10;

	while (!fn_at_command_saraG450("AT&K0", 100) && timeToScape != 0) {
		HAL_Delay(100);
		timeToScape--;
	}
	if (!fn_at_command_saraG450("AT", 100)) {
		fn_fprint("AT COMMAND DONT START");
		fn_power_off_sara();

	}
	if (!fn_at_command_saraG450("AT+CPIN?", 100)) {
		fn_fprint("CHIP DONT START");
		fn_power_off_sara();
	}
}

void fn_get_module_info_sara(void) { //call this one time if you need
	//  Manufacturer identification +CGMI
	fn_at_command_saraG450("AT+CGMI", 100);
	// Model identification +CGMM
	fn_at_command_saraG450("AT+CGMM", 100);
	//  Firmware version identification +CGMR
	fn_at_command_saraG450("AT+CGMR", 100);
	// IMEI identification +CGSN
	fn_at_command_saraG450("AT+CGSN", 100);

}

uint8_t fn_network_configuration_sara() {	//call this one time
	uint8_t responce = 0;
	fn_at_command_saraG450("AT+COPS?", 300);
	fn_at_command_saraG450("AT+CGATT?", 300);
	//  Packet switched data configuration +UPSD
	//DEFINE PROFILE 1 - VIVO
	//defines  the  APN  operator
	responce = fn_at_command_saraG450("AT+UPSD=1,1,\"zap.vivo.com.br\"", 50);
	//defines  the  APN username
	responce = fn_at_command_saraG450("AT+UPSD=1,2,\"vivo\"", 50);
	//defines  the  APN  password
	responce = fn_at_command_saraG450("AT+UPSD=1,3,\"vivo\"", 50);
	//DEFINE PROFILE 2 - TIM
	//defines  the  APN  operator
	responce = fn_at_command_saraG450("AT+UPSD=2,1,\"timbrasil.br\"", 50);
	//defines  the  APN username
	responce = fn_at_command_saraG450("AT+UPSD=2,2,\"tim\"", 50);
	//defines  the  APN  password
	responce = fn_at_command_saraG450("AT+UPSD=2,3,\"tim\"", 50);
	//DEFINE PROFILE 3 - CLARO
	//defines  the  APN  operator
	responce = fn_at_command_saraG450("AT+UPSD=3,1,\"claro.com.br\"", 50);
	//defines  the  APN username
	responce = fn_at_command_saraG450("AT+UPSD=3,2,\"claro\"", 50);
	//defines  the  APN  password
	responce = fn_at_command_saraG450("AT+UPSD=3,3,\"claro\"", 50);
	//DEFINE PROFILE 4 - OI
	//defines  the  APN  operator
	responce = fn_at_command_saraG450("AT+UPSD=4,1,\"oi.com.br\"", 50);
	//defines  the  APN username
	responce = fn_at_command_saraG450("AT+UPSD=4,2,\"oi\"", 50);
	//defines  the  APN  password
	responce = fn_at_command_saraG450("AT+UPSD=4,3,\"oi\"", 50);

	return responce;
}

uint8_t fn_network_activation_sara() {	//call ever time you reset the module
	uint8_t responce = 1;




//****** Check the network registration status ********************
	if (fn_at_command_word_responce_saraG450("AT+COPS?", 300, "+COPS: 0")
			== 0) {
		fn_at_command_saraG450("AT+COPS=0", 500);
		if (fn_at_command_word_responce_saraG450("AT+COPS?", 300, "+COPS: 0")
				== 0) {
			fn_at_command_saraG450("AT+COPS=0", 1000);
			if (fn_at_command_word_responce_saraG450("AT+COPS?", 300,
					"+COPS: 0") == 0) {
				fn_at_command_saraG450("AT+COPS=0", 2000);
			}
		}
	}



//****** Check the GPRS attach status. ********************
	fn_at_command_saraG450("AT+CGATT=1", 30000);
	fn_at_command_saraG450("AT", 300);
	if (fn_at_command_word_responce_saraG450("AT+CGATT?", 300, "+CGATT:0")
			== 1) {
		fn_at_command_saraG450("AT+CGATT=1", 5000);
		if (fn_at_command_word_responce_saraG450("AT+CGATT?", 300, "+CGATT:0")
				== 1) {
			fn_at_command_saraG450("AT+CGATT=1", 1000);
			if (fn_at_command_word_responce_saraG450("AT+CGATT?", 300,
					"+CGATT:0") == 1) {
				fn_at_command_saraG450("AT+CGATT=1", 2000);
			}
		}
	}

	//  Activate the GPRS connection.

	if (fn_at_command_word_responce_saraG450("AT+UPSDA=0,3", 500,
			"+UUPSDA: 0,\"\"") == 1) {
		//******Check the assigned IP address. ********************
		if (fn_at_command_word_responce_saraG450("AT+UPSND=0,0", 300,
				"+UPSND: 0,0,\"\"") == 1) {
			fn_at_command_saraG450("AT+UPSDA=0,3", 1000);
			if (fn_at_command_word_responce_saraG450("AT+UPSND=0,0", 300,
					"+UPSND: 0,0,\"\"") == 1) {
				fn_at_command_saraG450("AT+UPSDA=0,3", 1000);
			}
			if (fn_at_command_word_responce_saraG450("AT+UPSND=0,0", 300,
					"+UPSND: 0,0,\"\"") == 1) {
			}
		}
	}

	return responce;
}

void fn_http_mensage_sara(int type_mensage) {
	/*	//Delete fss file
	 fn_at_command_saraG450("AT+UDELFILE=\"data.fss\"", 200);*/
	//Reset the HTTP profile #0 OF 3
	switch (type_mensage) {
	case DAILY_UPDATE_FRAME:
		fn_at_command_saraG450("AT+UHTTP=0", 500);
		//Set the server domain name and port.
		fn_at_command_saraG450("AT+UHTTP=0,1,\"wizebox-api-dev.herokuapp.com\"",
				500);
		//Function mount the payload command
		fn_mount_frame_sara(DAILY_UPDATE_FRAME);
		//GET request of the default page
		fn_at_command_saraG450(uhttpc_comand_frame, 10000);

		// Check the server's reply data
		fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);

		if (get_downlink_payload((char*) aStringToReceiver_SARA) == 0) {
			fn_at_command_saraG450(uhttpc_comand_frame, 20000);
			fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);
			get_downlink_payload((char*) aStringToReceiver_SARA);
		}
		HAL_Delay(200);
		break;
	case INFO_FRAME_PROCESSED:
		fn_at_command_saraG450("AT+UHTTP=0", 500);
		//Set the server domain name and port.
		fn_at_command_saraG450("AT+UHTTP=0,1,\"wizebox-api-dev.herokuapp.com\"",
				500);
		//Function mount the payload command
		fn_mount_frame_sara(INFO_FRAME_PROCESSED);
		//GET request of the default page
		fn_at_command_saraG450(uhttpc_comand_frame, 10000);

		// Check the server's reply data
		fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);

		/*		if (get_downlink_payload((char*) aStringToReceiver_SARA) == 0) {
		 fn_at_command_saraG450(uhttpc_comand_frame, 20000);
		 fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);
		 get_downlink_payload((char*) aStringToReceiver_SARA);
		 }*/
		HAL_Delay(200);

		break;
	case ALERT_FRAME:
		fn_at_command_saraG450("AT+UHTTP=0", 500);
		//Set the server domain name and port.
		fn_at_command_saraG450("AT+UHTTP=0,1,\"wizebox-api-dev.herokuapp.com\"",
				500);
		//Function mount the payload command
		fn_mount_frame_sara(CONFIG_FRAME);
		//GET request of the default page
		fn_at_command_saraG450(uhttpc_comand_frame, 10000);

		// Check the server's reply data
		fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);

		if (get_downlink_payload((char*) aStringToReceiver_SARA) == 0) {
			fn_at_command_saraG450(uhttpc_comand_frame, 20000);
			fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);
			get_downlink_payload((char*) aStringToReceiver_SARA);
		}
		HAL_Delay(200);

		break;

	case TAGO_FRAME:
		//char *data= "\"Device-Token: 24438ecc-546d-4115-a10d-72ce8f1b20fa\"'{\"variable\":\"temperature\",\"value\":27,\"unit\":\"F\"}'";
		fn_at_command_saraG450("AT+UHTTP=0,6,1", 500);
		fn_at_command_saraG450("AT+UHTTP=0,1,\"https://api.tago.io\"", 500);

		//fn_at_command_saraG450("\"Content-Type: application/json\" \"Device-Token: 24438ecc-546d-4115-a10d-72ce8f1b20fa\"", 5000);
		//"-H "Content-Type: application/json" -H "Device-Token: 24438ecc-546d-4115-a10d-72ce8f1b20fa" -X POST -d '{"variable":"temperature","value":27,"unit":"F"}'"
		/*		fn_at_command_saraG450("AT+UHTTPC=0,4,\"api.tago.io/data\",\"result.txt\",\"postdata.txt\",4", 1000);
		 fn_at_command_saraG450("AT+URDFILE=\"result.txt\"", 20000);
		 fn_at_command_saraG450("AT+URDFILE=\"postdata.txt\"", 10000);

		 HAL_Delay(10000);

		 fn_at_command_saraG450("AT+UHTTPC=0,4,\"api.tago.io/data\",\"result.txt\",\"postdata.txt\",1", 1000);
		 fn_at_command_saraG450("AT+URDFILE=\"result.txt\"", 20000);
		 fn_at_command_saraG450("AT+URDFILE=\"postdata.txt\"", 10000);*/

		fn_at_command_saraG450(
				"AT+UHTTPC=0,5,\"https://api.tago.io/data\",\"result.txt\",\"Device-Token: 24438ecc-546d-4115-a10d-72ce8f1b20fa\",6,\"Content-Type: application/json\"",
				5000);
		fn_at_command_saraG450("AT+URDFILE=\"result.txt\"", 20000);
		//fn_at_command_saraG450("AT+URDFILE=\"postdata.txt\"", 10000);

		HAL_Delay(10000);

		fn_at_command_saraG450("AT+UDWNFILE=\"postdata.txt\",103", 1000);
		fn_at_command_saraG450(
				"\"Device-Token: 24438ecc-546d-4115-a10d-72ce8f1b20fa\"'{\"variable\":\"temperature\",\"value\":27,\"unit\":\"F\"}'",
				7000);

		fn_at_command_saraG450(
				"AT+UHTTPC=0,4,\"https://api.tago.io/data\",\"result.txt\",\"postdata.txt\",4",
				5000);
		fn_at_command_saraG450("AT+URDFILE=\"result.txt\"", 20000);
		fn_at_command_saraG450("AT+URDFILE=\"postdata.txt\"", 10000);

		HAL_Delay(10000);

		fn_at_command_saraG450(
				"AT+UHTTPC=0,4,\"https://api.tago.io/data\",\"result.txt\",\"postdata.txt\",1",
				5000);
		fn_at_command_saraG450("AT+URDFILE=\"result.txt\"", 20000);
		fn_at_command_saraG450("AT+URDFILE=\"postdata.txt\"", 10000);

		/*			//Function mount the payload command
		 fn_mount_frame_sara(TAGO_FRAME);
		 //GET request of the default page
		 fn_at_command_saraG450(uhttpc_comand_frame, 10000);

		 // Check the server's reply data
		 fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);

		 if (get_downlink_payload((char*) aStringToReceiver_SARA) == 0) {
		 fn_at_command_saraG450(uhttpc_comand_frame, 20000);
		 fn_at_command_saraG450("AT+URDFILE=\"data.fss\"", 1000);
		 get_downlink_payload((char*) aStringToReceiver_SARA);
		 }*/
		HAL_Delay(200);

		break;

	default:
		break;

	}
}

void fn_power_off_sara(void) {	//to power off call this
	fn_fprint("\r\nSARA MODULE SWITCH OFF\r\n");
	fn_at_command_saraG450("AT+CPWROFF", 200);
	HAL_Delay(100);
	GPRS_OFF
	HAL_Delay(100);
	GPRS_PWR_OFF
	HAL_Delay(100);
	fn_fprint("SARA MUDULE OFF\r\n");
}

/*
 *
 *
 * COMMAND FUNCIONS
 *
 *
 *
 */

/*
 * int fn_get_imei_sara()
 * if you need to check the imei number call this
 * the string number allocate in char sara_imei
 * return the number of imei in decimal int
 *
 */
int fn_get_imei_sara() {
	fn_at_command_saraG450("AT+CGSN", 100);
	int counter_mei = 0;
	int imei = 0;
	//char responce_sara[BYTES_TO_RECEIVER];
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var] > 47 && aStringToReceiver_SARA[var] < 58
				&& aStringToReceiver_SARA[var] != 0) {
			sara_imei[counter_mei] = aStringToReceiver_SARA[var];
			if (counter_mei == 15)
				break;
			counter_mei++;
			imei = 1;
		}
	}
	return imei;
}

/*
 * AUXILIAR FUNCIONS - PAYLOAD
 */

void fn_mount_frame_sara(int type_frame) {
	memset(st_frame_type.frame_report_values, 0, 22);
	memset(uhttpc_comand_frame, 0, 80);
	fn_get_imei_sara();
	fn_encoder_report_frame(st_frame_type.frame_report_values);

	switch (type_frame) {

	case DAILY_UPDATE_FRAME:
		strcpy(uhttpc_comand_frame,
				"AT+UHTTPC=0,1,\"/gprs/bidir?ack=true&device=");
		strcat(uhttpc_comand_frame, sara_imei);
		strcat(uhttpc_comand_frame, "&data=");
		strcat(uhttpc_comand_frame, st_frame_type.frame_report_values);
		strcat(uhttpc_comand_frame, "\",\"data.fss\"");
		break;
	case INFO_FRAME_PROCESSED:
		strcpy(uhttpc_comand_frame, "AT+UHTTPC=0,1,\"/sigfox/uplink?device=");
		strcat(uhttpc_comand_frame, sara_imei);
		strcat(uhttpc_comand_frame, "&data=");
		strcat(uhttpc_comand_frame, st_frame_type.frame_report_values);
		strcat(uhttpc_comand_frame, "\",\"data.fss\"");
		break;
	case TAGO_FRAME:
		strcpy(uhttpc_comand_frame,
				"AT+UHTTPC=0,5,\"[{\"variable\": \"payload_str\",\"value\": \"358327000000000000003300\"}]\",4,");
		/*		strcat(uhttpc_comand_frame, sara_imei);
		 strcat(uhttpc_comand_frame, "&data=");
		 strcat(uhttpc_comand_frame, st_frame_type.frame_report_values);
		 strcat(uhttpc_comand_frame, "\",\"data.fss\"");*/
		break;
	default:
		break;
	}
}

//*****************     UART    *********************

void Start_Sara_Transfers(char* info) {

	memset(aStringToSend_SARA, 0, 100);
	ubSizeToSend_SARA = strlen(info);
	for (int var = 0; var < ubSizeToSend_SARA; ++var) {
		aStringToSend_SARA[var] = info[var];
	}
	/* Start transfer only if not already ongoing */
	if (ubSend_SARA == 0) {
		/* Start USART transmission : Will initiate TXE interrupt after TDR register is empty */
		LL_USART_TransmitData8(USART5, aStringToSend_SARA[ubSend_SARA++]);
		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USART5);
	}

}

void USART5_CharTransmitComplete_Callback(void) {
	if (ubSend_SARA == ubSizeToSend_SARA) {
		ubSend_SARA = 0;

		/* Disable TC interrupt */
		LL_USART_DisableIT_TC(USART5);

	}
}

void USART5_TXEmpty_Callback(void) {
	if (ubSend_SARA == (ubSizeToSend_SARA - 1)) {
		/* Disable TXE interrupt */
		LL_USART_DisableIT_TXE(USART5);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USART5);
	}

	/* Fill TDR with a new char */
	LL_USART_TransmitData8(USART5, aStringToSend_SARA[ubSend_SARA++]);
}

void USART5_CharReception_Callback(void) {
	//__IO uint32_t received_char;

	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	//received_char = LL_USART_ReceiveData8(USART4);
	aStringToReceiver_SARA[ubSizeToReceiver_SARA++] = LL_USART_ReceiveData8(
	USART5);	//received_char;
	/* Check if received value is corresponding to specific one : \r */
	if ((LL_USART_ReceiveData8(USART5) == 13)) {
		flag_sara = 1;
		/* Turn LED2 On : Expected character has been received */
		/* Echo received character on TX */

		/* Clear Overrun flag, in case characters have already been sent to USART */
		LL_USART_ClearFlag_ORE(USART5);

	}

	//LL_USART_TransmitData8(USARTx_INSTANCE, received_char);
}

void fn_at_uart_reception_sara(void) {

	if (flag_sara) {
		ubSizeToReceiver_SARA = 0;
		HAL_UART_Transmit(&hlpuart1, aStringToReceiver_SARA,
		SARA_BYTES_TO_RECEIVER, 1000);
		flag_sara = 0;
	}

}

//********************************************************************************
/*
 * int fn_at_command_sara(char* at_command, int msDelay)
 * function send at command and counter millisecond delay (enter values)
 * Return 0 if the at command don't return OK and 1 if return OK
 */
uint8_t fn_at_command_saraG450(char* at_command, unsigned int msDelay) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;
	int size_at_command = strlen(at_command);
	size_at_command++;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r");

	Start_Sara_Transfers(new_command);
	HAL_Delay(msDelay);
	fn_at_uart_reception_sara();
	HAL_Delay(100);
	for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
		if (aStringToReceiver_SARA[var - 1] == 79
				&& aStringToReceiver_SARA[var] == 75) {
			atReturn = 1;
		}
	}

	return atReturn;
}

uint8_t fn_at_command_word_responce_saraG450(char* at_command, uint32_t msDelay,
		char* responce) {
	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);
	uint8_t atReturn = 0;

	int size_at_command = strlen(at_command);
	size_at_command++;
	char new_command[size_at_command];
	strcpy(new_command, at_command);
	strcat(new_command, "\r");

	Start_Sara_Transfers(new_command);
	HAL_Delay(500);
	fn_at_uart_reception_sara();

	int size_responce = strlen(responce);
	char new_responce[size_responce];
	strcpy(new_responce, responce);

	atReturn = find_word((char*) aStringToReceiver_SARA, new_responce);

	return atReturn;
}

uint8_t fn_check_sarag450_status() {
	uint8_t check = 0, count = 10;
	GPRS_ON
	HAL_Delay(1000);
	GPRS_RESET_OFF
	HAL_Delay(3000);
	GPRS_PWR_ON

	memset(aStringToReceiver_SARA, 0, SARA_BYTES_TO_RECEIVER);

	char new_command[6] = "AT&K0\r";
	while (count) {
		count--;
		Start_Sara_Transfers(new_command);
		HAL_Delay(500);
		if (flag_sara) {
			ubSizeToReceiver_SARA = 0;
			flag_sara = 0;
		}

		for (int var = 0; var < SARA_BYTES_TO_RECEIVER; var++) {
			if (aStringToReceiver_SARA[var - 1] == 79
					&& aStringToReceiver_SARA[var] == 75) {
				check = 1;
				count = 0;
			}
		}
	}
	fn_fprint("\r\nSARA IMEI:");
	// IMEI identification +CGSN
	fn_at_command_saraG450("AT+CGSN", 100);
	GPRS_OFF
	HAL_Delay(100);
	GPRS_PWR_OFF
	return check;
}
